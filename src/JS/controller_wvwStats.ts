let startTimer = Date.now()

import {addItemBulk, dataToDbArray, getItems, liftDB, Db} from "../config/db"
import {Account_Game, WvW_Compacted, WvW_Manager, WvW_Manager_lastData, WvW_Raw} from "../config/db_entities";
import {userTiers} from "../config/config"
import Proxy from "../classes/Proxy"
import {getNextUpdate} from "./functions"

const proxy = new Proxy()

let timer = { accounts: [] }

let zeroed = new Date(new Date().setUTCHours(0, 0, 0, 0))
let oneWeekAgo:any = new Date().setDate(zeroed.getDate() - 7)
oneWeekAgo = new Date(oneWeekAgo).toISOString()

controller().then(() => console.log("Complete")).catch((err) => console.log(err));
async function controller () {
  // test the proxy
  await proxy.test();

  let {db, client} = await liftDB()
  timer["connection"] = (Date.now() - startTimer) / 1000

  let accounts: WvW_Manager[] = await getItems<WvW_Manager>(db, "wvw_manager", { nextUpdate: { $lte: new Date().toISOString() }, active: { $eq: 0 } }).catch((err) => {console.log(err);return []})

  let accounts_processed = await getWvwData(accounts, db)
  timer["getWvwStats"] = (Date.now() - startTimer) / 1000

  // process the raw stats
  await processRawStats(db, accounts_processed)
  timer["processRawStats"] = (Date.now() - startTimer) / 1000

  await deleteOlderData(db, accounts_processed)
  timer["deleteOlderData"] = (Date.now() - startTimer) / 1000

  timer["finish"] = (Date.now() - startTimer) / 1000

  await client.close()

  console.log(timer)
}
async function getWvwData (accounts: WvW_Manager[], db: Db):  Promise<WvW_Manager[]> {
  let accountManagerTmp: Partial<WvW_Manager>[] = []
  let wvwStatsRawTmp: WvW_Raw[] = []
  let accountsTmp: WvW_Manager[] = []

  let updates = []
  for (let i = 0; i < accounts.length; i++) {
    let account = accounts[i]

    let account_details_raw: Account_Game[] = await getItems<Account_Game>(db, "accounts_game", {id: account.id}, {}).catch((err) => {console.log(err);return []})
    if (account_details_raw.length === 0) {continue}
    let account_details = account_details_raw[0]

    // flags it so other scripts dont pull it
    let newAccount = false
    if (account.age === 0) {newAccount = true}
    if (typeof account.nextUpdate === 'undefined') { continue }

    updates.push({
      id: account.id,
      nextUpdate: getNextUpdate(account_details.level, newAccount, accounts[i].nextUpdate, userTiers),
      active: 0
    })
  }

  await addItemBulk(db, "wvw_manager", dataToDbArray(updates)).catch((err) => {console.log(err)})

  for (let i = 0; i < accounts.length; i++) {
    let account = accounts[i]

    timer.accounts.push(account.id)

    let account_details_raw: Account_Game[] = await getItems<Account_Game>(db, "accounts_game", {id: account.id}, {}).catch((err) => {console.log(err);return []})
    if (account_details_raw.length === 0) {continue}
    let account_details = account_details_raw[0]

    let date = new Date().toISOString()
    let tmp = { characterDataAccount: {} }
    let characters = account.characters

    // section that gets the data
    // get v2/account
    tmp = await getAccount(tmp, account_details)

    if (tmp["errorMsg"] === 'ErrTimeout') {
      accountManagerTmp.push({
        active: 0,
        id: tmp["id"]
      })
      continue
    }

    // if the account has been used it proceeds here
    if (typeof tmp["age"] !== 'undefined' && tmp["age"] > account.age) {
      // get v2/account/achievements
      tmp = await getAchievements(tmp, account_details)
      // get v2/characters
      tmp = await getCharacters(tmp, account_details)
      // for each character get v2/characters/<name>
      for (let j = 0; j < tmp["characters"].length; j++) {
        tmp = await getCharacter(tmp, account_details, tmp["characters"][j])
      }
    }

    // handles errors first
    if (tmp["error"]) {
      // if (tmp.errorMsg  === 'invalid key') {
      let errorCount = account.errorCount || 0
      let breakpoint = (24 * 2) / (userTiers[account_details.level]) + 1

      // mark invalid
      if (errorCount > breakpoint) {
        accountManagerTmp.push({
          active: 2,
          id: tmp["id"]
        })
      } else {
        // incriment errorCount
        accountManagerTmp.push({
          active: 0,
          id: tmp["id"],
          errorCount: errorCount++
        })
      }

      // }
      continue
    }

    // if the age is the same nothing happened and no stats pulled
    // schedule next pull
    if (tmp["age"] === account.age) {
      accountManagerTmp.push({
        active: 0,
        id: tmp["id"],
        lastUpdate: date,
        errorCount: 0
      })
      continue
    }

    // if no wvw stuff happened then update teh account age but not add more data
    let lastData = { world: tmp["world"], commander: tmp["commander"], wvw_rank: tmp["wvw_rank"], achievements: tmp["achievements"] }
    if (compareLastData(account.lastData, lastData)) {
      accountManagerTmp.push({
        active: 0,
        lastData: lastData,
        id: tmp["id"],
        age: tmp["age"],
        characters: characters,
        lastUpdate: date,
        errorCount: 0
      })
      continue
    }

    // these accounts have new data thus can be added to the processing queue
    accountsTmp.push(account)
    let characterKeys = Object.keys(tmp.characterDataAccount)
    for (let j = 0; j < characterKeys.length; j++) {
      characters[characterKeys[j]] = tmp.characterDataAccount[characterKeys[j]]
    }

    accountManagerTmp.push({
      active: 0,
      lastData: lastData,
      id: tmp["id"],
      age: tmp["age"],
      characters: characters,
      lastUpdate: date,
      errorCount: 0
    })
    wvwStatsRawTmp.push({
      id: date + '_' + tmp["id"],
      time: date,
      user: tmp["id"],
      age: tmp["age"],
      world: tmp["world"],
      commander: tmp["commander"],
      wvw_rank: tmp["wvw_rank"],
      achievements: tmp["achievements"],
      characterData: characters
    })

    // when it reaches 20 process and reset
    if(accountManagerTmp.length > 20){
      await addItemBulk(db, "wvw_manager", dataToDbArray(accountManagerTmp, 25000))
      accountManagerTmp = []
    }
    if(wvwStatsRawTmp.length > 20){
      await addItemBulk(db, "wvw_raw", dataToDbArray(wvwStatsRawTmp, 25000))
      wvwStatsRawTmp = []
    }
  }
  return accountsTmp
}

async function getAccount (tmp, account_details: Account_Game) {
  let dataRequest = await proxy.get(`https://api.guildwars2.com/v2/account?access_token=${account_details.key}`).catch(err => {console.log(err); return {status:500}})

  if (typeof dataRequest.data.text !== 'undefined' || dataRequest.status !== 200) {
    tmp.error = true
    tmp.errorMsg = dataRequest.data.text
    return tmp
  }
  let dataResult = dataRequest.data
  tmp.id = dataResult.name
  tmp.age = dataResult.age
  tmp.world = dataResult.world
  tmp.commander = dataResult.commander
  tmp.wvw_rank = dataResult.wvw_rank
  return tmp
}

async function getAchievements (tmp, account_details: Account_Game) {
  let dataRequest = await proxy.get(`https://api.guildwars2.com/v2/account/achievements?access_token=${account_details.key}`).catch(err => {console.log(err); return {status:500}})

  if (typeof dataRequest.data.text !== 'undefined' || dataRequest.status !== 200) {
    tmp.error = true
    tmp.errorMsg = dataRequest.data.text
    return tmp
  }
  tmp.achievements = {}
  let dataResult = dataRequest.data
  for (let i = 0; i < dataResult.length; i++) {
    if (dataResult[i].id === 283) {
      tmp.achievements.kills = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 285) {
      tmp.achievements.yakDefend = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 288) {
      tmp.achievements.yakCapture = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 291) {
      tmp.achievements.campCapture = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 294) {
      tmp.achievements.stoneMistCapture = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 297) {
      tmp.achievements.towerCapture = dataResult[i].current
      continue
    }

    if (dataResult[i].id === 300) {
      tmp.achievements.keepCapture = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 303) {
      tmp.achievements.objectiveCapture = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 306) {
      tmp.achievements.supplySpentRepair = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 310) {
      tmp.achievements.campDefend = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 313) {
      tmp.achievements.stoneMistDefend = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 316) {
      tmp.achievements.keepDefend = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 319) {
      tmp.achievements.objectiveDefend = dataResult[i].current
      continue
    }
    if (dataResult[i].id === 322) {
      tmp.achievements.towerDefend = dataResult[i].current
    }
  }

  return tmp
}

async function getCharacters (tmp, account_details: Account_Game) {
  let dataRequest = await proxy.get(`https://api.guildwars2.com/v2/characters?access_token=${account_details.key}`).catch(err => {console.log(err); return {status:500}})

  if (typeof dataRequest.data.text !== 'undefined' || dataRequest.status !== 200) {
    tmp.error = true
    tmp.errorMsg = dataRequest.data.text
    tmp.characters = []
    return tmp
  }
  tmp.characters = dataRequest.data
  return tmp
}

async function getCharacter (tmp, account_details: Account_Game, character) {
  let dataRequest = await proxy.get(`https://api.guildwars2.com/v2/characters/${character}/core?access_token=${account_details.key}`).catch(err => {console.log(err); return {status:500}})

  if (typeof dataRequest.data.text !== 'undefined' || dataRequest.status !== 200) {
    tmp.error = true
    tmp.errorMsg = dataRequest.data.text
    tmp.characters = []
    return tmp
  }
  let dataResult = dataRequest.data
  if (typeof tmp.characterDataAccount === 'undefined') {
    tmp.characterDataAccount = {}
  }
  tmp.characterDataAccount[dataResult.created + '_' + character] = {
    age: dataResult.age,
    deaths: dataResult.deaths
  }

  if (typeof tmp.characterData === 'undefined') {
    tmp.characterData = {}
  }
  tmp.characterData[character] = {
    age: dataResult.age,
    deaths: dataResult.deaths
  }
  return tmp
}

interface Difference {
  [propName: string]:WvW_Compacted
}

async function processRawStats (db: Db, accounts: WvW_Manager[]) {
  let updateArray: WvW_Compacted[] = []
  let achievements = ["kills", "yakDefend", "yakCapture", "campCapture", "stoneMistCapture", "towerCapture", "keepCapture", "objectiveCapture", "supplySpentRepair", "campDefend", "stoneMistDefend", "keepDefend", "objectiveDefend", "towerDefend"]
  for (let i = 0; i < accounts.length; i++) {
    let account = accounts[i]

    let stats: WvW_Raw[] = await getItems<WvW_Raw>(db, "wvw_raw", { user: account.id }).catch((err) => {console.log(err);return []})
    let statsSorted = sorting<WvW_Raw>(stats, 'time')
    let differenceData: Difference = {}
    for (let j = 0; j < statsSorted.length; j++) {
      if (j + 1 < statsSorted.length) {
        let time0 = statsSorted[j + 1].time
        time0 = time0.slice(0, 10)

        if (typeof differenceData[time0] === 'undefined') {
          differenceData[time0] = {
            id: time0 + '_' + statsSorted[j].user,
            date: new Date(new Date(time0).setUTCHours(0, 0, 0, 0)).toISOString(),
            user: statsSorted[j].user,
            age: 0,
            wvw_rank: 0,
            kills: 0,
            yakDefend: 0,
            yakCapture: 0,
            campCapture: 0,
            stoneMistCapture: 0,
            towerCapture: 0,
            keepCapture: 0,
            objectiveCapture: 0,
            supplySpentRepair: 0,
            campDefend: 0,
            stoneMistDefend: 0,
            keepDefend: 0,
            objectiveDefend: 0,
            towerDefend: 0,
            characters: {}
          }
        }

        if(statsSorted[j].age && statsSorted[j + 1].age){differenceData[time0].age += statsSorted[j + 1].age - statsSorted[j].age}
        if(statsSorted[j].wvw_rank && statsSorted[j + 1].wvw_rank){differenceData[time0].wvw_rank += statsSorted[j + 1].wvw_rank - statsSorted[j].wvw_rank}

        if(statsSorted[j].achievements && statsSorted[j + 1].achievements){
          for(let achievement of achievements){
            if(statsSorted[j].achievements[achievement] && statsSorted[j + 1].achievements[achievement]){
              differenceData[time0][achievement] += statsSorted[j + 1].achievements[achievement] - statsSorted[j].achievements[achievement]
            }
          }
        }

        let characters = Object.keys(statsSorted[j + 1].characterData)
        for (let k = 0; k < characters.length; k++) {
          if (typeof differenceData[time0].characters[characters[k]] === 'undefined') {
            differenceData[time0].characters[characters[k]] = { age: 0, deaths: 0 }
          }
          let age = 0
          let deaths = 0
          if (typeof statsSorted[j].characterData[characters[k]] !== 'undefined') {
            age = statsSorted[j].characterData[characters[k]].age
            deaths = statsSorted[j].characterData[characters[k]].deaths
          }
          differenceData[time0].characters[characters[k]].age += statsSorted[j + 1].characterData[characters[k]].age - age // || statsSorted[j + 1].characterData[characters[k]].age || 0
          differenceData[time0].characters[characters[k]].deaths += statsSorted[j + 1].characterData[characters[k]].deaths - deaths // || statsSorted[j + 1].characterData[characters[k]].deaths || 0
        }
      }
    }

    // clearing out misc data
    let days = Object.keys(differenceData)
    for (let j = 0; j < days.length; j++) {
      let categories = Object.keys(differenceData[days[j]])
      for (let k = 0; k < categories.length; k++) {
        if (differenceData[days[j]][categories[k]] === 0) {
          delete differenceData[days[j]][categories[k]]
        }
        if (categories[k] === 'characters') {
          let characters = Object.keys(differenceData[days[j]][categories[k]])
          for (let l = 0; l < characters.length; l++) {
            if (differenceData[days[j]][categories[k]][characters[l]].age === 0) {
              delete differenceData[days[j]][categories[k]][characters[l]]
            }
          }
        }
      }
      if (Object.keys(differenceData[days[j]]).length > 4) {
        updateArray.push(differenceData[days[j]])
      }
    }
  }

  await addItemBulk(db, "wvw_compacted", dataToDbArray(updateArray))
}

function sorting<T> (array, attribute, reverse?): T[] {
  return array.sort(function (a, b) {
    let attrA = a[attribute]
    let attrB = b[attribute]

    if (reverse) {
      if (attrA < attrB) { return 1 }
      if (attrA > attrB) { return -1 }
    } else {
      if (attrA < attrB) { return -1 }
      if (attrA > attrB) { return 1 }
    }

    return 0
  })
}

async function deleteOlderData (db, accounts) {
  for (let i = 0; i < accounts.length; i++) {
    await db.collection('wvw_raw').deleteMany({ user: accounts[i].id, time: { $lt: oneWeekAgo } }).catch((err) => { console.log(err) })
  }
}

function compareLastData (oldData:  WvW_Manager_lastData, newData:  WvW_Manager_lastData) {
  if (typeof oldData === 'undefined' || typeof newData === 'undefined') { return false }
  // if they have equal values it returrns true.
  let equality = true
  let mainKeys = ['world', 'commander', 'wvw_rank']
  let achievmentKeys = ['kills', 'yakDefend', 'yakCapture', 'campCapture', 'towerCapture', 'keepCapture', 'objectiveCapture', 'supplySpentRepair', 'campDefend', 'stoneMistDefend', 'keepDefend', 'objectiveDefend', 'towerDefend']

  for (let i = 0; i < mainKeys.length; i++) {
    if (typeof oldData[mainKeys[i]] !== 'undefined' && typeof newData[mainKeys[i]] !== 'undefined') {
      if (oldData[mainKeys[i]] !== newData[mainKeys[i]]) {
        equality = false
        break
      }
    }
  }
  if (oldData.achievements && newData.achievements) {
    for (let i = 0; i < achievmentKeys.length; i++) {
      if (oldData.achievements[achievmentKeys[i]] && newData.achievements[achievmentKeys[i]]) {
        if (oldData.achievements[achievmentKeys[i]] !== newData.achievements[achievmentKeys[i]]) {
          equality = false
          break
        }
      }
    }
  }
  return equality
}
