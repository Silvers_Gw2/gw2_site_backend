let startTimer = Date.now()

import axios from "axios"
import { liftDB, getItems, addItemBulk, dataToDbArray, Db, addItem } from "../config/db"
import { Account_Game, Transaction_Manager, WvW_Manager, Account } from "../config/db_entities";
import { patreon } from "../config/config"

let timer = {}
controller().then(() => console.log("Complete")).catch((err) => console.log(err));
async function controller () {
  let {db, client} = await liftDB()
  timer["connection"] = (Date.now() - startTimer) / 1000

  await setPatreonInfo(db)
  timer["setPatreonInfo"] = (Date.now() - startTimer) / 1000

  await setPrivateInfo(db)
  timer["setPrivateInfo"] = (Date.now() - startTimer) / 1000

  await setCustomInfo(db)
  timer["setCustomInfo"] = (Date.now() - startTimer) / 1000

  await setAccountsLevel(db)
  timer["setAccountsLevel"] = (Date.now() - startTimer) / 1000

  timer["finish"] = (Date.now() - startTimer) / 1000
  console.log(timer)

  await client.close()
}

function getAccountTierPermanent (donation, patreonCurrent, patreonLifetime) {
  let tier

  // Figure out tier from current cents
  let monthly = patreonCurrent / 100

  if (monthly === 0) {
    tier = 1
  }
  if (monthly > 0 && monthly <= 1) {
    tier = 2
  }
  if (monthly > 1) {
    tier = 3
  }

  let total = donation.private + (patreonLifetime / 100)
  if (total >= 12 && total < 24) {
    if (tier < 2) { tier = 2 }
  }
  if (total >= 24) {
    if (tier < 3) { tier = 3 }
  }

  if (typeof donation.custom !== 'undefined') {
    if (donation.custom > tier) {
      tier = donation.custom
    }
  }
  return tier
}

async function setCustomInfo (db: Db) {
  let accounts = await getItems<Account>(db, "accounts", { 'donation.custom': { $gt: 0 } }, { _id: 0 })

  let updates: Partial<Account>[] = []
  for (let i = 0; i < accounts.length; i++) {
    if (accounts[i].donation.custom > accounts[i].accountLevel) {
      updates.push({
        user: accounts[i].user,
        accountLevel: accounts[i].donation.custom
      })
    }
  }
  let batched = dataToDbArray(updates, undefined, "user")
  await addItemBulk(db, "accounts", batched).catch((err) => {console.log(err)})
}

async function setPrivateInfo (db: Db) {
  let accounts = await getItems<Account>(db, "accounts", { 'donation.private': { $gt: 0 } }, { _id: 0 })

  let updates: Partial<Account>[] = []
  for (let i = 0; i < accounts.length; i++) {
    let privateLevel = getAccountTierPermanent(accounts[i].donation, 0, 0)
    if (privateLevel > accounts[i].accountLevel) {
      updates.push({
        user: accounts[i].user,
        accountLevel: privateLevel
      })
    }
  }
  let batched = dataToDbArray(updates, undefined, "user")
  await addItemBulk(db, "accounts", batched).catch((err) => {console.log(err)})
}

interface Patreon_user_attributes {
  email?: string;

  currently_entitled_amount_cents: number;
  lifetime_support_cents: number;

  patron_status: string;
}

interface Patreon_user {
  attributes: Patreon_user_attributes;
  id: string;
  type: string;
}

interface Patreon_res {
  data: Patreon_user[];
  links?: {
    first?: string;
    prev?: string;

    next?: string;
    last?: string;
  }
}


async function get_patreon(): Promise<Patreon_user[]>{
  let result = [];

  // first one will always run
  let next = `https://www.patreon.com/api/oauth2/v2/campaigns/${patreon.page}/members?fields%5Bmember%5D=email,lifetime_support_cents,currently_entitled_amount_cents,patron_status`;

  while (next){
    let page: Patreon_res = await axios({ url: next, headers: { Authorization: `Bearer ${patreon.key}` } }).then(response => response.data ).catch((err) => {console.log(err);return {data:[]}})
    result.push(...page.data);

    if (page.links){
      // set the next page, if it exists
      next = page.links.next;
    } else {
      next = undefined;
    }
  }

  return result
}

async function setPatreonInfo (db: Db) {
  let patreon = await get_patreon();

  let updates: Partial<Account>[] = []
  for (let i = 0; i < patreon.length; i++) {
    let {email, currently_entitled_amount_cents, lifetime_support_cents} = patreon[i].attributes;

    if(email === null){continue}

    let accounts = await getItems<Account>(db, "accounts", {email: email}, {_id: 0})

    // no account of that email
    if (accounts.length === 0) { continue }
    // account exists, calculate new tier
    let account = accounts[0]
    let newTier = getAccountTierPermanent(account.donation, currently_entitled_amount_cents, lifetime_support_cents)
    // tier the same, no change
    if (newTier === account.accountLevel) { continue }

    let donation = account.donation
    donation.patreon = lifetime_support_cents
    updates.push({
      user: account.user,
      accountLevel: newTier,
      donation: donation,
    })
  }
  let batched = dataToDbArray(updates, undefined, "user")
  await addItemBulk(db, "accounts", batched).catch((err) => {console.log(err)})
}

interface Accounts_Game {
  [propName: string]:{
    _accountLevel: number
    accountLevel: number
    _removed: boolean
    removed: boolean
  }
}
async function setAccountsLevel (db: Db) {
  let accounts_game_tmp = await getItems<Account_Game>(db, "accounts_game", {}, {id:1, level: 1, removed: 1})
  let accounts_game:Accounts_Game = {}

  for (let i = 0; i < accounts_game_tmp.length; i++) {
    accounts_game[accounts_game_tmp[i].id] = {
      // original
      _accountLevel: accounts_game_tmp[i].level,
      // new
      accountLevel: 1,
      // if it was previously marked as removed
      _removed: accounts_game_tmp[i].removed,
      // all entries are initally counted as removed
      removed: true
    }
  }

  let allAccounts = await getItems<Account>(db, "accounts", {}, {gameAccounts: 1, accountLevel:1,changed:1 })

  for (let i = 0; i < allAccounts.length; i++) {
    let userAccount = allAccounts[i]
    let gameIDs = userAccount.gameAccounts

    for (let j = 0; j < gameIDs.length; j++) {
      if (typeof accounts_game[gameIDs[j]] !== 'undefined') {
        // as long as someone has it on their account it remains valid
        accounts_game[gameIDs[j]].removed = false

        if (userAccount.accountLevel > accounts_game[gameIDs[j]].accountLevel) {
          accounts_game[gameIDs[j]].accountLevel = userAccount.accountLevel
        }
      }
    }
  }

  let updates: Partial<Account_Game>[] = []
  for (let [id, details] of Object.entries(accounts_game)) {
    let tmp: Partial<Account_Game> = {
      id: id,
      level: details.accountLevel,
      removed: details.removed
    }

    if(
        details.accountLevel === details._accountLevel &&
        details.removed === details._removed
    ){
      // no change
      continue
    }

    // if anytthign ahs changed update transactions and wvw
    if(details.removed){
      // thios fires if the account is removed

      await addItem<Transaction_Manager>(db, "transaction_manager", { id: id, active: 1 }).catch(err => console.log(err))
      await addItem<WvW_Manager>(db, "wvw_manager", { id: id, active: 1 }).catch(err => console.log(err))
    }else{
      // this makes sure the account remains active

      await addItem<Transaction_Manager>(db, "transaction_manager", { id: id, active: 0 }).catch(err => console.log(err))
      await addItem<WvW_Manager>(db, "wvw_manager", { id: id, active: 0 }).catch(err => console.log(err))
    }

    updates.push(tmp)
  }

  let batched = dataToDbArray(updates)
  await addItemBulk(db, "accounts_game", batched).catch((err) => {console.log(err)})
}
