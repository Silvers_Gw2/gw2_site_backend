let startTimer = Date.now()

import { liftDB, getItems } from "../config/db"
import { LoginRewards_Manager } from "../config/db_entities";
import Proxy from "../classes/Proxy"
import { getLoginRewards } from "./functions"

const proxy = new Proxy()

let timer = { accounts: [] }

controller().then(() => console.log("Complete")).catch((err) => console.log(err));
async function controller () {
  // test the proxy
  await proxy.test();

  let {db, client} = await liftDB()
  timer["connection"] = (Date.now() - startTimer) / 1000

  // get all accounts with a next update older than now, excludes new accounts
  let accounts: LoginRewards_Manager[] = await getItems<LoginRewards_Manager>(db, "LoginRewards_Manager", { nextUpdate: { $lte: new Date().toISOString() }, active: { $eq: 0 } }, undefined, undefined, 50).catch((err) => {console.log(err);return []})


  let complete = 0;
  let requests = accounts.map(account => getLoginRewards(account, db, false, proxy).then(() => {
    complete += 1;
    console.log(`Complete: ${complete}/${accounts.length}`);
  }))

  await Promise.all(requests).catch(err => console.log(err))

  timer["finish"] = (Date.now() - startTimer) / 1000

  await client.close();
  console.log(timer);
}