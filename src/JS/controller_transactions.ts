let startTimer = Date.now()

import {liftDB, getItems, addItem, addItemBulk, Db, dataToDbArray} from "../config/db"
import {Account_Game, Transaction_Compacted, Transaction_Manager, Transaction_Normal} from "../config/db_entities";
import {userTiers} from "../config/config"
import Proxy from "../classes/Proxy"
import {getNextUpdate} from "./functions"

const proxy = new Proxy()

let zeroed = new Date(new Date().setUTCHours(0, 0, 0, 0))
let oneMonthAgo:any = new Date().setDate(zeroed.getDate() - 9)
oneMonthAgo = new Date(oneMonthAgo).toISOString()

let timer = { accounts: [] }

controller().then(() => console.log("Complete")).catch((err) => console.log(err));
async function controller () {
  // test the proxy
  await proxy.test();

  let {db, client} = await liftDB()
  timer["connection"] = (Date.now() - startTimer) / 1000

  // get all accounts with a next update older than now, excludes new accounts
  let accounts: Transaction_Manager[] = await getItems<Transaction_Manager>(db, "transaction_manager", { nextUpdate: { $lte: new Date().toISOString() }, firstRun: { $eq: 2 }, active: { $eq: 0 } }, undefined, undefined, 5).catch((err) => {console.log(err);return []})

  await getTransactions(accounts.reverse(), db, false)
  timer["getTransacationsOld"] = (Date.now() - startTimer) / 1000

  // This picks up all teh new accounts
  let newAccounts: Transaction_Manager[] = await getItems<Transaction_Manager>(db, "transaction_manager", { nextUpdate: { $lte: new Date().toISOString() }, firstRun: { $eq: 0 }, active: { $eq: 0 } }, undefined, undefined, 5).catch((err) => {console.log(err);return []})

  await getTransactions(newAccounts, db, true)
  timer["getTransacationsNew"] = (Date.now() - startTimer) / 1000

  timer["finish"] = (Date.now() - startTimer) / 1000

  await client.close()
  console.log(timer)
}

async function getTransactions (accounts: Transaction_Manager[], db: Db, newAccount: boolean) {
  for (let i = 0; i < accounts.length; i++) {
    console.log(`${i}/${accounts.length}`)
    let scrapeStart = new Date().toISOString()
    timer.accounts.push(accounts[i].id)

    // set up details
    let account = accounts[i]

    let account_details_raw: Account_Game[] = await getItems<Account_Game>(db, "accounts_game", {id: account.id}, {}).catch((err) => {console.log(err);return []})
    if (account_details_raw.length === 0) {continue}
    let account_details = account_details_raw[0]
    let breakpoint = (24 * 2) / (userTiers[account_details.level]) + 1

    // handle new accounts
    if (newAccount) {
      // 1 means it is being processed and it wont be caught up in teh next db sweep
      await addItem(db, "transaction_manager", {id: account.id, firstRun: 1}).catch((err) => {console.log(err)})
    }

    // handle accounts that errored out before
    if (account.errorCount > breakpoint) {
      await db.collection('transaction_manager').updateOne({id: account.id}, {$set: {active: 2}}, {upsert: true}).catch(async (err) => {console.log(err)})
      continue
    }

    let buyData = await processTransactions(account, account_details, 'buys')
    let sellData = await processTransactions(account, account_details, 'sells')

    if (buyData["error"] || sellData["error"]) {
      if (
          buyData["errorMsg"] === 'invalid key' ||
          sellData["errorMsg"] === 'invalid key' ||

          buyData["errorMsg"] === 'ErrTimeout' ||
          sellData["errorMsg"] === 'ErrTimeout'
      ) {
        await db.collection('transaction_manager')
            .findOneAndUpdate(
                {id: account.id},
                {
                  $inc: {errorCount: 1},
                  $set: {nextUpdate: getNextUpdate(account_details.level, false, account.nextUpdate, userTiers)}
                }
            ).catch((err) => {console.log(err)})
        continue
      }
    }

    let updateArrayItemDataBuy = dataToDbArray(buyData["data"], 25000)
    await addItemBulk(db, "transaction_buy", updateArrayItemDataBuy)

    let updateArrayItemDataSell = dataToDbArray(sellData["data"], 25000)
    await addItemBulk(db, "transaction_sell", updateArrayItemDataSell)

    // creates compacted data
    await compactCharacter(db, account.id, 'buy')
    await compactCharacter(db, account.id, 'sell')

    // deletes older non compacted data
    await deleteOlderData(db, account.id)

    let tempSet: Transaction_Manager = {
      id: account.id,
      lastUpdate: scrapeStart,
      nextUpdate: getNextUpdate(account_details.level, false, account.nextUpdate, userTiers),
      lastBuyID: buyData["lastID"] || account['lastBuyID'],
      lastSellID: sellData["lastID"] || account['lastSellID'],
      // if it works reset teh error count
      errorCount: 0,
      active: 0,
      firstRun: 2,
      initialPages: {
        buy: buyData["initialPages"] || 0,
        sell: sellData["initialPages"] || 0
      }
    }

    // Sets teh vars for scraping, what time to go back to as well as what time to do the next scrape.
    // Also flags it as done first run
    await db.collection('transaction_manager').updateOne({id: account.id}, {'$set': tempSet}, {upsert: true}).catch(err => console.log(err))
  }
}

async function processTransactions (account:Transaction_Manager, account_details: Account_Game, type: string) {
  let initial = await proxy.get(`https://api.guildwars2.com/v2/commerce/transactions/history/${type}?page_size=200&page=0&access_token=${account_details.key}`).catch(err => console.log(err))

  if (typeof initial.data.text !== 'undefined' || initial.status !== 200) {
    return {
      error: true,
      errorMsg: initial.data.text,
      data: []
    }
  }

  let temp = []
  let maxID = 1
  for (let j = 0; j < initial.data.length; j++) {
    // get teh enw max ID
    if (initial.data[j].id > maxID) { maxID = initial.data[j].id }

    // always add the first page to teh db
    temp.push(initial.data[j])
  }

  let lastID = (type === 'buys') ? account['lastBuyID'] : account['lastSellID']
  if (!lastID) { lastID = 1 }

  // give a 3 page buffer
  let pages_buffer = 0;
  for (let j = 1; j < initial.headers['x-page-total']; j++) {
    let page = await proxy.get(`https://api.guildwars2.com/v2/commerce/transactions/history/${type}?page_size=200&page=${j}&access_token=${account_details.key}`).catch(err => console.log(err))

    if (page.status !== 200 || typeof page.data.text !== 'undefined') { continue }

    // go an entire page of
    let flag = 0;
    for (let k = 0; k < page.data.length; k++) {
      // get teh new max ID
      if (page.data[k].id > maxID) { maxID = page.data[k].id }

      // add all entries
      temp.push(page.data[k])

      if (page.data[k].id < lastID) {
        flag += 1;
      }
    }

    if (flag === page.data.length) {
      pages_buffer += 1;

      if (pages_buffer === 3){
        break
      }
    }
  }

  let latestDate = (initial.data.length > 0) ? initial.data[0].purchased : new Date().toISOString()

  let result: Transaction_Normal[] = temp.map(item => {
    item.created = new Date(item.created).toISOString();
    item.purchased = new Date(item.purchased).toISOString();
    item.user = account.id;
    return item
  })

  return {
    initialPages: initial.headers['x-page-total'],
    latestDate: latestDate,
    lastID: maxID,
    data: result
  }
}

interface Days {
  [propName: string]:Transaction_Compacted
}
async function compactCharacter (db: Db, character_id: string, method: string) {
  let transactions: Transaction_Normal[] = await getItems<Transaction_Normal>(db, `transaction_${method}`, { user: character_id }).catch((err) => {console.log(err);return []})
  let days:Days = {}
  for (let i = 0; i < transactions.length; i++) {
    let day_tmp:any = new Date(transactions[i].purchased)
    day_tmp.setUTCHours(0, 0, 0, 0)

    let day = day_tmp.toISOString()
    if (typeof days[day] === 'undefined') {
      days[day] = {
        id: character_id + '_' + day,
        user: character_id,
        date: day,
        ids: [],
        data: {}
      }
    }

    let id = transactions[i].item_id
    let price = transactions[i].price
    let quantity = transactions[i].quantity
    let orderTime = transactions[i].created
    let successTime = transactions[i].purchased

    if (typeof days[day]['data'][id] === 'undefined') {
      days[day]['data'][id] = {
        firstTime: orderTime,
        quantity: 0,
        totalPrice: 0,
        transactions: 0,
        totalTime: 0
      }
      days[day]['ids'].push(id)
    }
    if (days[day]['data'][id]['firstTime'] > orderTime) {
      days[day]['data'][id]['firstTime'] = orderTime
    }
    days[day]['data'][id]['quantity'] += quantity
    days[day]['data'][id]['totalPrice'] += (quantity * price)
    days[day]['data'][id]['transactions'] += 1
    days[day]['data'][id]['totalTime'] += (new Date(successTime).getTime() - new Date(orderTime).getTime()) / 1000
  }

  let daysArray = Object.values(days)
  let result = dataToDbArray(daysArray,25000)
  await addItemBulk(db, `transaction_${method}_compacted`, result)
}

async function deleteOlderData (db: Db, character: string) {
  await db.collection('transaction_buy').deleteMany({ user: character, purchased: { $lt: oneMonthAgo } }).catch((err) => {console.log(err)})
  await db.collection('transaction_sell').deleteMany({ user: character, purchased: { $lt: oneMonthAgo } }).catch((err) => {console.log(err)})
}
