// This is to convert from display name to the guid

import {addItem, Db, getItems, liftDB, dataToDbArray, addItemBulk} from "../config/db";
import Proxy from "../classes/Proxy"
import {Account_Game} from "../config/db_entities";

const proxy = new Proxy()


main().then(()=> console.log("fin")).catch(err=> console.log(err))
async function main() {
    console.time("Conversion");
    let {db, client} = await liftDB(true)

    await proxy.test();

    // get all accounts in the database
    let accounts = await getItems(db, "accounts", {}).catch((err) => {console.log(err);return []})

    // handle each account one by one

     for(let i=0;i<accounts.length;i++){
         let account = accounts[i]
         console.log(i, accounts.length, account.user)
        if(typeof account.gameAccounts !== "object"){continue}

        // gets teh ID's of the connected accounts
        let {ids, accountMatch} = await createGameAccountTable(db, account["gameAccounts"]).catch(err => {console.log(err);return {ids: [], accountMatch: []}})
        account.gameAccounts = ids

        // use the AccountMatch to convert old records
        await convertTransactions(db, accountMatch)

        await convertWvW(db, accountMatch)

        await addItem(db,"accounts", account, "user").catch(err => console.log(err))
    }

    client.close()
    console.timeEnd("Conversion");
}


interface AccountMatch {
    name: string
    id: string
}
async function createGameAccountTable(db: Db, gameAccounts): Promise<{ids:string[], accountMatch: AccountMatch[]}> {
    let ids = []
    let accountMatch: AccountMatch[] = []


    for (let [oldDisplayName, value] of Object.entries(gameAccounts)) {
        let key = value["key"]

        // get the required data
        let gw2Account = await proxy.get(`https://api.guildwars2.com/v2/account?access_token=${key}`).catch(err => {console.log(err);return {status: 500, error: err}})
        if (gw2Account.status !== 200) {continue}


        let permissions = await proxy.get(`https://api.guildwars2.com/v2/tokeninfo?access_token=${key}`).catch(err => {console.log(err);return {status: 500, error: err}})
        if (permissions.status !== 200) {continue}

    // still enforcing requiring all permissions
        if (permissions.data.permissions.length < 10) {continue}

        // the data for teh new colle tion
        let newAccount:Account_Game = {
            id: gw2Account.data.id,
            key: key,
            displayName: gw2Account.data.name,
            created: gw2Account.data.created,
            firstAdded: new Date().toISOString(),
            access: gw2Account.data.access,
            permissions: permissions.data.permissions,
            level: 0
        }

        await addItem(db,"accounts_game", newAccount).catch(err => console.log(err))
        ids.push(gw2Account.data.id)

        accountMatch.push({name: oldDisplayName, id: gw2Account.data.id})
    }

    return {ids, accountMatch}
}

async function convertTransactions (db: Db, accountMatch: AccountMatch[]) {
    for(let i=0;i<accountMatch.length;i++){
        let account = accountMatch[i]

        // transactionManager
        let transactionManager = await getItems(db, "transactionManager", {id: account.name}).catch((err) => {console.log(err);return []})
        if(transactionManager.length === 1){
            // into transaction_manager

            let transactionManager_new = transactionManager[0]
            delete transactionManager_new._id
            delete transactionManager_new.apiKey
            delete transactionManager_new.accountLevel
            transactionManager_new.id = account.id

            await addItem(db,"transaction_manager", transactionManager_new).catch(err => console.log(err))
        }

        // transactionBuy
        let transactionBuy = await getItems(db, "transactionBuy", {user: account.name}).catch((err) => {console.log(err);return []})
        if(transactionBuy.length > 0){
            let transactionBuy_new = []
            for(let j=0;j<transactionBuy.length;j++){
                let transactionBuy_tmp = transactionBuy[j]
                delete transactionBuy_tmp._id
                transactionBuy_tmp.user = account.id
                transactionBuy_new.push(transactionBuy_tmp)
            }

            let bulked = dataToDbArray(transactionBuy_new)

            // into transaction_buy
            await addItemBulk(db, "transaction_buy", bulked).catch(err => console.log(err))
        }

        // transactionBuyCompacted
        let transactionBuyCompacted = await getItems(db, "transactionBuyCompacted", {user: account.name}).catch((err) => {console.log(err);return []})
        if(transactionBuyCompacted.length > 0){
            let tmp_array = []
            for(let j=0;j<transactionBuyCompacted.length;j++){
                let tmp = transactionBuyCompacted[j]
                delete tmp._id
                tmp.user = account.id
                tmp.id = transactionBuyCompacted[j].id.replace(account.name, account.id)
                tmp_array.push(tmp)
            }
            let bulked = dataToDbArray(tmp_array)
            await addItemBulk(db, "transaction_buy_compacted", bulked).catch(err => console.log(err))
        }

        // transactionSell
        let transactionSell = await getItems(db, "transactionSell", {user: account.name}).catch((err) => {console.log(err);return []})
        if(transactionSell.length > 0){
            let transactionBuy_new = []
            for(let j=0;j<transactionSell.length;j++){
                let transactionSell_tmp = transactionSell[j]
                delete transactionSell_tmp._id
                transactionSell_tmp.user = account.id
                transactionBuy_new.push(transactionSell_tmp)
            }

            let bulked = dataToDbArray(transactionBuy_new)

            // into transaction_buy
            await addItemBulk(db, "transaction_sell", bulked).catch(err => console.log(err))
        }

        // transactionSellCompacted

        let transactionSellCompacted = await getItems(db, "transactionSellCompacted", {user: account.name}).catch((err) => {console.log(err);return []})
        if(transactionSellCompacted.length > 0){
            let tmp_array = []
            for(let j=0;j<transactionSellCompacted.length;j++){
                let tmp = transactionSellCompacted[j]
                delete tmp._id
                tmp.user = account.id
                tmp.id = transactionSellCompacted[j].id.replace(account.name, account.id)
                tmp_array.push(tmp)
            }
            let bulked = dataToDbArray(tmp_array)
            await addItemBulk(db, "transaction_sell_compacted", bulked).catch(err => console.log(err))
        }

    }


}

async function convertWvW (db: Db, accountMatch: AccountMatch[]){
    for(let i=0;i<accountMatch.length;i++) {
        let account = accountMatch[i]

        // wvwStatsManager
        let wvwStatsManager = await getItems(db, "wvwStatsManager", {id: account.name}).catch((err) => {console.log(err);return []})
        if(wvwStatsManager.length === 1){
            // into transaction_manager

            let transactionManager_new = wvwStatsManager[0]
            delete transactionManager_new._id
            delete transactionManager_new.apiKey
            delete transactionManager_new.accountLevel
            transactionManager_new.id = account.id

            await addItem(db,"wvw_manager", transactionManager_new).catch(err => console.log(err))
        }



        let wvwStatsRaw = await getItems(db, "wvwStatsRaw", {character: account.name}).catch((err) => {console.log(err);return []})
        if(wvwStatsRaw.length > 0){
            let transactionBuy_new = []
            for(let j=0;j<wvwStatsRaw.length;j++){
                let transactionBuy_tmp = wvwStatsRaw[j]
                delete transactionBuy_tmp._id
                delete transactionBuy_tmp.character
                transactionBuy_tmp.id = wvwStatsRaw[j].id.replace(account.name, account.id)
                transactionBuy_tmp.user = account.id
                transactionBuy_new.push(transactionBuy_tmp)
            }

            let bulked = dataToDbArray(transactionBuy_new)

            // into transaction_buy
            await addItemBulk(db, "wvw_raw", bulked).catch(err => console.log(err))
        }

        let wvwStats = await getItems(db, "wvwStats", {character: account.name}).catch((err) => {console.log(err);return []})
        if(wvwStats.length > 0){
            let transactionBuy_new = []
            for(let j=0;j<wvwStats.length;j++){
                let transactionBuy_tmp = wvwStats[j]
                delete transactionBuy_tmp._id
                delete transactionBuy_tmp.character
                transactionBuy_tmp.id = wvwStats[j].id.replace(account.name, account.id)
                transactionBuy_tmp.user = account.id
                transactionBuy_new.push(transactionBuy_tmp)
            }

            let bulked = dataToDbArray(transactionBuy_new)

            // into transaction_buy
            await addItemBulk(db, "wvw_compacted", bulked).catch(err => console.log(err))
        }
    }
}