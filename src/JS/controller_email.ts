let startTimer = Date.now()

import nodemailer from "nodemailer"
import axios from "axios";
import {liftDB, getItems,  addItemBulk, dataToDbArray} from "../config/db"
import { email as Email} from "../config/config"

let transporter = nodemailer.createTransport({ pool: true, host: Email.host, port: 465, secure: true, auth: { user: Email.user, pass: Email.pass }, tls: {rejectUnauthorized: false} })

let hourAgo = new Date(new Date().getTime() - (1000 * 60 * 65)).toISOString()
// hourAgo = "2019-11-05T00:00:02.948Z"
let timer = {}

controller().then(() => console.log("Complete")).catch((err) => console.log(err));
async function controller () {
  let latestBuild = await axios.get('https://api.datawars2.ie/gw2/v1/build/json?limit=1').catch((err) => {console.log(err); return {status:500}})
  if (latestBuild.status !== 200) { return }
  let build = latestBuild["data"][0]
  timer["build"] = (Date.now() - startTimer) / 1000

  let {db, client} = await liftDB()
  timer["connection"] = (Date.now() - startTimer) / 1000

  // notification of new

  // basically check to see if a build happened in the last 35 min and if so it continues
  // some email changes will run hourly
  if (build.iso < hourAgo) {
    console.log(timer)
    await client.close()
    return
  }

  await gemstoreListNotificationsMaster(db)
  timer["gemstoreListNotificationsMaster"] = (Date.now() - startTimer) / 1000

  // notification of new gemstore items

  console.log(timer)
  await client.close()
}

async function gemstoreListNotificationsMaster (db) {
  let collection = 'gemStoreLists'
  let catalogueRaw = await axios.get('https://api.datawars2.ie/gw2/v1/gemstore/catalogue/json?fields=uuid,dataId,description,latestEndDate,latestStartDate,name,gemPrice').catch(err => {console.log(err); return {status: 500}})
  if (catalogueRaw.status !== 200) { return }
  let catalogue = catalogueRaw["data"]
  let temp = {}
  for (let i = 0; i < catalogue.length; i++) {
    temp[catalogue[i].uuid] = catalogue[i]
  }

  let limit = 10
  let count = await db.collection(collection).find({}).count()
  for (let i = 0; i < count; i = i + limit) {
    let accounts = await getItems(db, collection, {}, { _id: 0, firstAdded: 0 }, i, limit).catch(err => {console.log(err); return []})
    await gemstoreListNotifications(db, accounts, temp, collection)
  }
}

async function gemstoreListNotifications (db, accounts, catalogue, collection) {
  let updates = []
  for (let i = 0; i < accounts.length; i++) {
    let user = accounts[i].user
    let items = accounts[i].items

    // check if teh account has an email attached, if it dosent it skips
    let accountMain = await getItems(db, 'accounts', { user: user }, { _id: 0, email: 1 }).catch(err => {console.log(err); return []})
    if(accountMain.length ===0){continue}
    let email = accountMain[0].email
    if (typeof email === 'undefined') { continue }
    if (email === '') { continue }

    let returningItems = []
    for (let j = 0; j < items.length; j++) {
      let uuid = items[j].uuid
      let itemData = catalogue[uuid]
      if (itemData.latestStartDate === null) { continue }
      if (items[j].lastDate < itemData.latestStartDate) {
        returningItems.push(itemData)
        items[j].lastDate = new Date().toISOString()

        items[items.findIndex(x => x.uuid === uuid)] = items[j]
      }
    }

    if (returningItems.length === 0) { continue }

    updates.push({
      user: user,
      items: items || []
    })

    // returningItems is the items that are going to be emailed
    await gemstoreListNotificationsEmail(user, email, returningItems)
  }

  let batched = dataToDbArray(updates, undefined, "user", {firstAdded: new Date().toISOString()})
  await addItemBulk(db, collection, batched).catch((err) => {console.log(err)})
}

async function gemstoreListNotificationsEmail (user, email, returningItems) {
  let head = "<head><style>table {border-collapse: collapse;text-align:left; }table, th, td { border: 1px solid black;text-align:left;}</style><meta charset='utf-8'><title>DataWars2 Gemstore Notifier</title><meta name='description' content='DataWars2 Gemstore Notifier'></head>"
  let tableStart = '<table><tr><th>Name</th><th>Last GemPrice</th><th>From</th><th>To</th><th>Description</th></tr>'
  let tableMiddle = ''
  let tableEnd = '</table>'

  for (let i = 0; i < returningItems.length; i++) {
    returningItems[i].latestStartDate = new Date(returningItems[i].latestStartDate).toUTCString()
    returningItems[i].latestEndDate = new Date(returningItems[i].latestEndDate).toUTCString()
    tableMiddle += '<tr><td>' + returningItems[i].name + '</td><td>' + returningItems[i].gemPrice + '</td><td>' + returningItems[i].latestStartDate + '</td><td>' + returningItems[i].latestEndDate + '</td><td>' + returningItems[i].description + '</td></tr>'
  }
  let table = tableStart + tableMiddle + tableEnd
  let currentTime = new Date().toUTCString()

  let body = '<body><div><h4>Hi ' + user + '!</h4><p>These items have returned to the Gemstore:<br />' + table + '<br />GemPrice is manually entered by Gw2Efficiency so it may not reflect current prices. (I am using their prices for this as its currently the only way, lend them your support if you can) <br />This tool is not affiliated with Gw2Efficiency<br />Current server time is ' + currentTime + '</p><p>DataWars2<br /></p><hr /><p>If you need to change the watchlist please return to <a href="https://datawars2.ie/gemstore_notifier" target="_blank" rel="noopener noreferrer">Gemstore Notifier</a>  <br/>This address (gemstore_notifier@datawars2.ie) does not support replies, please use support@datawars2.ie for support/queries.</p></div></body>'

  let html = "<!doctype html><html lang='en'>" + head + body + '</html>'
  let message = {
    from: {
      name: 'DataWars2',
      address: 'gemstore_notifier@datawars2.ie'
    },
    to: email,
    subject: 'DataWars2 - Gemstore Notifier',
    html: html
  }

  await transporter.sendMail(message).catch((err) => {console.log(err)})
}