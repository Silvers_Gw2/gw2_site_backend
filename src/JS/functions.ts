import {
  Account_Game,
  Accounts_sessions,
  LoginRewards_Manager,
  LoginRewards,
  LoginRewardsItems
} from "../config/db_entities";
import {getItems, addItem, Db} from "../config/db"
import {userTiers} from "../config/config";
import Proxy from "../classes/Proxy";

export async function deleteItems (db, collection, deletionQuery) {
  await db.collection(collection).deleteMany(deletionQuery).catch((err) => { console.log('error', err); return null })
}

export function dateAgo (type, quantity, customStart?, seconds?) {
  let date = new Date()
  if (typeof type !== 'string') { return date.toISOString() }
  if (typeof customStart === 'string') { date = new Date(customStart) }

  switch (type) {
    case 'minutes': {
      date.setMinutes(date.getMinutes() - quantity)
      date.setUTCSeconds(0, 0)
      break
    }
    case 'hours': {
      date.setHours(date.getHours() - quantity)
      date.setUTCMinutes(0, 0, 0)
      break
    }
    case 'days': {
      date.setDate(date.getDate() - quantity)
      date.setUTCHours(0, 0, 0, 0)
      break
    }
    default: {}
  }

  if (typeof seconds === 'number') {
    date.setUTCSeconds(seconds, 0)
  }
  return date.toISOString()
}

export async function addToDB (db, collection, dbArray) {
  for (let i = 0; i < dbArray.arrays + 1; i++) {
    if (dbArray[i].length > 0) {
      await db.collection(collection).bulkWrite(dbArray[i], { ordered: false }).catch((err) => { console.log('error', collection + ': ' + err) })
    }
  }
}

export function getNextUpdate (level, newAccount, old, mapping, offset: number = 0) {
  let randomHour = 0
  let randomMinute = 0
  if (newAccount) {
    randomHour = Math.floor(Math.random() * 23)
    randomMinute = Math.floor(Math.random() * 59)
  }

  if(level < 1){level = 1;}
  if(level > 4){level = 4;}

  return new Date((new Date().getTime() + (((mapping[level] - randomHour) * 60) - randomMinute) * 60000) + offset).toISOString()
}

class verifySessionObject extends Object{
  result:string
  success?: string
  error?: string
}
export async function verifySession (db: Db, session: string, req):Promise<verifySessionObject> {
  let existing = await getItems<Accounts_sessions>(db, 'accounts_sessions', { session: session })
  let result:verifySessionObject = {result:undefined}
  if (existing.length > 0) {
    result["result"] = 'success'
    result["success"] = existing[0].user

    let tmp: Partial<Accounts_sessions> = {
      session: session,
      lastAccessed:new Date().toISOString(),
      logins: existing[0].logins + 1,
      userAgent: req.headers['user-agent'],
      ip: req.headers['x-forwarded-for']
    }
    await addItem<Accounts_sessions>(db, 'accounts_sessions', tmp, "session").catch((err) => {console.log(err)})
  } else {
    result["result"] = 'error'
    result["error"] = 'Invalid Session'
  }
  return result
}

export function beautifyJSON (body, flag) {
  if (flag === 'min' || flag === 'human') {
    if (flag === 'min') {
      return JSON.stringify(body)
    }
    if (flag === 'human') {
      return JSON.stringify(body, null, '  ')
    }
  } else {
    return JSON.stringify(body, null, '  ')
  }
}

export async function createUniqueSession (db: Db, user: string, req): Promise<string> {
  let randomString = [...Array(145)].map(() => (~~(Math.random() * 36)).toString(36)).join('')
  let existing = await getItems<Accounts_sessions>(db, 'accounts_sessions', { session: randomString })
  if (existing.length > 1) {
    randomString = await createUniqueSession(db, user, req)
  }
  let tmp: Accounts_sessions = {
    session: randomString,
    user:user,
    firstAccessed: new Date().toISOString(),
    ip: req.headers['x-forwarded-for'],
    userAgent: req.headers['user-agent'],
    userAgentInitial: req.headers['user-agent'],
    logins: 1,
    lastAccessed: new Date().toISOString()
  }
  await addItem<Accounts_sessions>(db, 'accounts_sessions', tmp, "session").catch((err) => {console.log(err)})

  return randomString
}

// for getting teh laurels of a user
export async function getLoginRewards (account_manager: LoginRewards_Manager, db: Db, newAccount: boolean, proxy: Proxy) {
  let accounts: Account_Game[] = await getItems<Account_Game>(db, "accounts_game", {id: account_manager.id}).catch((err) => {console.log(err);return []})

  let updateManager: Partial<LoginRewards_Manager> = {
    id: account_manager.id,
    lastUpdate: new Date().toISOString(),
    errorCount: account_manager.errorCount
  }

  let last_account;
  // one user can have multiple game accounts
  for(let account of accounts){
    last_account = account;


    try {
      let inventory = await processInventory(account, proxy);
      // bank
      let bank = await processGeneral(account, proxy, "bank");
      // mat storage
      let mat = await processGeneral(account, proxy, "materials");

      let wallet = await processWallet(account, proxy);

      let merged = mergeTogether(account, inventory, bank, mat, wallet);

      // add them to tehir respective DB's
      await db.collection('LoginRewards').updateOne({id: merged.id}, {'$set': merged}, {upsert: true}).catch(err => console.log(err))
    } catch (err){
      console.log(err)
      updateManager.errorCount += 1;
    }
  }

  if(last_account != null){
    let breakpoint = (24 * 2) / (userTiers[last_account.level]) + 1;

    if (updateManager.errorCount > breakpoint) {
      await db.collection('LoginRewards_Manager').updateOne({id: updateManager.id}, {$set: {active: 2}}, {upsert: true}).catch(async (err) => {console.log(err)})
      return;
    }

    updateManager.nextUpdate= getNextUpdate(last_account.level - 1, newAccount, account_manager.nextUpdate, userTiers);

    if(newAccount){
      updateManager.active = 0;
      updateManager.errorCount = 0;
      updateManager.firstAdded = new Date().toISOString()
      // redo it with a better offset, in teh future
      updateManager.nextUpdate = getNextUpdate(last_account.level - 1, newAccount, account_manager.nextUpdate, userTiers, 24 * 60 * 60 * 1000);
    }

    await db.collection('LoginRewards_Manager').updateOne({id: updateManager.id}, {'$set': updateManager}, {upsert: true}).catch(err => console.log(err))
  }
}

interface Inventory {
  bags: Array<Bag | null>
}
interface Bag {
  id: number;
  size: number
  inventory: Array<Items | null>
}
interface Items {
  id: number;
  count: number;
  binding?: string
  bound_to?: string;
}

interface WalletItems {
  id: number;
  value: number;
}
async function processWallet (account: Account_Game, proxy: Proxy): Promise<number> {
  let wallet: WalletItems[] = await proxy.get(`https://api.guildwars2.com/v2/account/wallet?access_token=${account.key}`).then((result) => result.data).catch(err => {console.log(err);return []})

  let result = 0;
  for (let currency of wallet) {
    if (currency.id === 3) {
      result = currency.value;
      break
    }
  }
  return result;
}

async function processInventory (account: Account_Game, proxy: Proxy): Promise<LoginRewardsItems> {
  // get all game accounts

  // easy to add items with intellij
  let bags: LoginRewardsItems = {19976: 0, 43766: 0, 68314: 0, 68315: 0, 68316: 0, 68317: 0, 68318: 0, 68319: 0, 68320: 0, 68321: 0, 68322: 0, 68323: 0, 68324: 0, 68325: 0, 68327: 0, 68328: 0, 68329: 0, 68330: 0, 68331: 0, 68332: 0, 68333: 0, 68334: 0, 68335: 0, 68336: 0, 68337: 0, 68338: 0, 68339: 0, 68340: 0, 68341: 0, 68350: 0, 68351: 0, 68352: 0, 68354: 0, 68326: 0};
  let keys = Object.keys(bags);
  
  let characters: String[] = await proxy.get(`https://api.guildwars2.com/v2/characters?access_token=${account.key}`).then((result) => result.data).catch(err =>  {console.log(err);return []})

  for(let character of characters){
    let inventory: Inventory = await proxy.get(`https://api.guildwars2.com/v2/characters/${character}/inventory?access_token=${account.key}`).then((result) => result.data).catch(err =>  {console.log(err);return {bags: []}})

    for(let bag of inventory.bags){
      if (bag === null){
        continue;
      }

      for(let item of bag.inventory){
        if (item === null){
          continue;
        }
        
        if(keys.indexOf(String(item.id)) != -1){
          if(typeof bags[item.id] === "undefined"){
            bags[item.id] = 0;
          }
          bags[item.id] += 1;
        }
      }
    }
  }

  return bags;
}

async function processGeneral (account: Account_Game, proxy: Proxy, type: string): Promise<LoginRewardsItems> {
  // easy to add items with intellij
  let bags: LoginRewardsItems = {19976: 0, 43766: 0, 68314: 0, 68315: 0, 68316: 0, 68317: 0, 68318: 0, 68319: 0, 68320: 0, 68321: 0, 68322: 0, 68323: 0, 68324: 0, 68325: 0, 68327: 0, 68328: 0, 68329: 0, 68330: 0, 68331: 0, 68332: 0, 68333: 0, 68334: 0, 68335: 0, 68336: 0, 68337: 0, 68338: 0, 68339: 0, 68340: 0, 68341: 0, 68350: 0, 68351: 0, 68352: 0, 68354: 0, 68326: 0};
  let keys = Object.keys(bags);

  let bank: Array<Items | null> = await proxy.get(`https://api.guildwars2.com/v2/account/${type}?access_token=${account.key}`).then((result) => result.data).catch(err =>  {console.log(err);return []})

  for(let item of bank){
    if (item === null){
      continue;
    }

    if(keys.indexOf(String(item.id)) != -1){
      if(typeof bags[item.id] === "undefined"){
        bags[item.id] = 0;
      }
      bags[item.id] += 1;
    }
  }

  return bags;
}

function mergeTogether (account: Account_Game, inventory: LoginRewardsItems,  bank: LoginRewardsItems,  mat: LoginRewardsItems,  wallet: number): LoginRewards{
  // use as base
  let merged_items = inventory;

  for (const [key, value] of Object.entries(bank)) {
    merged_items[key] += value;
  }
  for (const [key, value] of Object.entries(mat)) {
    merged_items[key] += value;
  }

  return{
    id: account.id,
    displayName: account.displayName,
    lastUpdate: new Date().toISOString(),

    wallet_laurel: wallet,
    ...merged_items

  }
}
