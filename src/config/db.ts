import {MongoClient, Db, Sort} from "mongodb"
import {db} from "./config";
import {indexes, collections} from "./db_setup";

export {MongoClient, Db}

export async function liftDB (index:boolean=false){
    console.time("Connecting to Database");
    let client = await MongoClient.connect(db.uri).catch(async(err) => {err.misc = "MongoDB failed to connect, exiting"; await console.log(err); process.exit(1) });
    let database = client.db(db.database);
    console.timeEnd("Connecting to Database");

    // these only run if the index.ts is run
    if(index){
        console.time("Creating: Collections");
        await collections(database);
        console.timeEnd("Creating: Collections");

        console.time("Creating: Indexes");
        await indexes(database);
        console.timeEnd("Creating: Indexes");
    }

    return {db: database, client}
}

export async function addItem<T>(db: Db, collection:string, item:Partial<T>, field:string = "id"){
    if(!collection){return}
    let find = {
        [field]: item[field]
    }
    return await db.collection(collection).updateOne(find, {$set: item}, {upsert: true})
}

export async function addItemBulk<T> (db: Db, collection: string,  dbArray: DataToDbArray<T>, location:Error = new Error()){
    if(!collection){return}
    for (let i = 0; i < dbArray.arrays + 1; i++) {
        if (dbArray[i].length > 0) {
            await db.collection(collection).bulkWrite(dbArray[i], { ordered: false }).catch((err) => console.log(collection, err,location))
        }
    }
}

interface UpdateOne<T>{
    updateOne: {
        filter: {
            [propName: string]: any
        }
        update: {
            $set: Partial<T>
            $setOnInsert?: Partial<T>
        }
        upsert: boolean
    }
}
interface DataToDbArray<T>{
    arrays: number
    [propName: number] : UpdateOne<T>[]
}
export function dataToDbArray<T> (data:Array<Partial<T>>, limit:number= 25000, accessor:string = "id", setOnInsert?:Partial<T>): DataToDbArray<T> {
    let tmp: DataToDbArray<T> = { arrays: 0, 0: [] };
    for (let j = 0; j < data.length; j++) {
        let temp: UpdateOne<T> = {
            updateOne: {
                filter:{
                    [accessor]: data[j][accessor]
                },
                update: {
                    $set: data[j]
                },
                upsert: true
            }
        };

        if (setOnInsert) {
            temp.updateOne.update["$setOnInsert"] = setOnInsert
        }
        if (tmp[tmp.arrays].length < limit) {
            tmp[tmp.arrays].push(temp)
        } else {
            tmp.arrays += 1;
            tmp[tmp.arrays] = [temp]
        }
    }
    return tmp
}


export async function deleteItem(db, collection:string, item:string, field:string = "id"){
    if(!collection){return}
    let find = {};
    find[field] = item;
    return await db.collection(collection).deleteOne(find);
}
export async function deleteItems (db: Db, collection:string, deletionQuery:object) {
    await db.collection(collection).deleteMany(deletionQuery)
}

export async function getItems<T>(db: Db, collection:string,find:object = {}, project:object = {_id:0}, skip:number=0, limit:number =0,sort:Sort = {}):Promise<Array<T>>{
    if(!collection){return []}
    return (db
        .collection(collection)
        .find(find)
        .project(project)
        .skip(skip)
        .limit(limit)
        .sort(sort)
        .toArray()
        .catch((err) => {console.log(err);return [];})
    ) as unknown as T[]
}