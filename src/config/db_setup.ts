
// this creates teh collections
export const collections = async (db)=>{

    await db.createCollection("accounts").catch(()=>{});
    await db.createCollection("accounts_game").catch(()=>{});
    await db.createCollection("accounts_sessions").catch(()=>{});

    await db.createCollection("resetPassword").catch(()=>{});
    await db.createCollection("gemStoreLists").catch(()=>{});

    await db.createCollection("verification").catch(()=>{});
    await db.createCollection("verification_cache").catch(()=>{});

    await db.createCollection("transaction_manager").catch(()=>{});
    await db.createCollection("transaction_buy").catch(()=>{});
    await db.createCollection("transaction_sell").catch(()=>{});
    await db.createCollection("transaction_buy_compacted").catch(()=>{});
    await db.createCollection("transaction_sell_compacted").catch(()=>{});

    await db.createCollection("transaction_sell_compacted").catch(()=>{});
    await db.createCollection("wvw_raw").catch(()=>{});
    await db.createCollection("wvw_compacted").catch(()=>{});

    await db.createCollection("LoginRewards_Manager").catch(()=>{});
    await db.createCollection("LoginRewards").catch(()=>{});
};

// this creates teh indexes
export const indexes = async (db)=>{
    await db.collection('transaction_manager').createIndex({ 'guid': 1 }).catch(()=>{});
    await db.collection('transaction_manager').createIndex({ 'email': 1 }).catch(()=>{});
    await db.collection('transaction_manager').createIndex({ 'guid': 1, password: 1 }).catch(()=>{});
    await db.collection('transaction_manager').createIndex({ 'nextUpdate': 1, firstRun: 1 }).catch(()=>{});
    await db.collection('transaction_manager').createIndex({ 'nextUpdate': 1, firstRun: 1, active: 1 }).catch(()=>{});

    await db.collection('transaction_buy').createIndex({ 'id': 1 }).catch(()=>{});
    await db.collection('transaction_sell').createIndex({ 'id': 1 }).catch(()=>{});

    await db.collection('transaction_sell_compacted').createIndex({ 'user': 1 }).catch(()=>{});
    await db.collection('transaction_buy_compacted').createIndex({ 'user': 1 }).catch(()=>{});
    await db.collection('transaction_sell_compacted').createIndex({ 'user': 1, date: 1, active: 1 }).catch(()=>{});
    await db.collection('transaction_buy_compacted').createIndex({ 'user': 1, date: 1, active: 1 }).catch(()=>{});

    await db.collection('wvw_manager').createIndex({ 'nextUpdate': 1, active: 1 }).catch(()=>{});
    await db.collection('wvw_manager').createIndex({ 'id': 1 }).catch(()=>{});
    await db.collection('wvw_raw').createIndex({ 'id': 1 }).catch(()=>{});
    await db.collection('wvw_compacted').createIndex({ 'id': 1 }).catch(()=>{});
    await db.collection('wvw_compacted').createIndex({ 'user': 1, 'date': 1 }).catch(()=>{});
    await db.collection('wvw_raw').createIndex({ 'user': 1 }).catch(()=>{});

    await db.collection('accounts').createIndex({ 'user': 1 }).catch(()=>{});
    await db.collection('accounts').createIndex({ 'email': 1 }).catch(()=>{});

    await db.collection('verification_cache').createIndex({ 'id': 1 }).catch(()=>{});
    await db.collection('verification').createIndex({ 'id': 1 }).catch(()=>{});

    await db.collection('resetPassword').createIndex({ 'user': 1 }).catch(()=>{});
    await db.collection('resetPassword').createIndex({ 'key': 1 }).catch(()=>{});
    await db.collection('resetPassword').createIndex({ 'expires': 1 }).catch(()=>{});

    await db.collection('gemStoreLists').createIndex({ 'user': 1 }).catch(()=>{});

    await db.collection('accounts_sessions').createIndex({ 'session': 1 }).catch(()=>{});

    await db.collection('LoginRewards_Manager').createIndex({ 'id': 1 }).catch(()=>{});
    await db.collection('LoginRewards_Manager').createIndex({ 'nextUpdate': 1 }).catch(()=>{});
    await db.collection('LoginRewards').createIndex({ 'id': 1 }).catch(()=>{});
};

