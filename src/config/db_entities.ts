import {Db} from "./db";
import restify from "restify";

export interface Account_Game {
    id: string
    key: string
    displayName: string
    created: string
    firstAdded?: string
    access: string[]
    permissions: string[]
    level: number
    removed?: boolean
}

export interface Account_base {
    user: string
    accountLevel: number
    donation: {
        private: number
        patreon: number
        custom: number
    }
    firstAdded?: string
    password: string
    email:string
    lists: {
        [propName: string]: string
    }
    emailDetails: {
        account: string
        password: string
    }
    verification:{
        [propName: string]: string
    }
}

export interface Account extends Account_base {
    gameAccounts: Account_Game["id"][]
}

// base for manager
export interface Manager {
    id: string
    active: number
    errorCount: number
    firstAdded?: string
    lastUpdate: string
    nextUpdate: string
}

export interface Transaction_Manager extends Manager{
    firstRun: number
    initialPages: {
        buy: number
        sell: number
    }

    lastBuyID: number
    lastSellID: number
}

export interface Transaction_Normal {
    id: number
    created: string
    item_id: number
    price: number
    purchased: string
    quantity: number
    user: string
}


export interface Transaction_Compacted {
    id: string
    data: {
        [propName: string]: Transaction_Compacted_Data
    }
    date: string
    ids: number[]
    user: string
}
interface Transaction_Compacted_Data {
    firstTime: string
    quantity: number
    totalPrice: number
    transactions: number
    totalTime: number
}


export interface WvW_Manager extends Manager {
    age: number
    characters: WvW_characters
    lastData:WvW_Manager_lastData
}

interface WvW_characters {
    [propName: string]: {
        age: number
        deaths: number
    }
}

export interface WvW_Manager_lastData {
    world: number
    commander: boolean
    wvw_rank: number
    achievements: WvW_achievements
}
interface WvW_achievements {
    kills:number
    yakDefend:number
    yakCapture:number
    campCapture:number
    stoneMistCapture:number
    towerCapture:number
    keepCapture:number
    objectiveCapture:number
    supplySpentRepair:number
    campDefend:number
    stoneMistDefend:number
    keepDefend:number
    objectiveDefend:number
    towerDefend:number
}

export interface WvW_Raw {
    id: string
    achievements: WvW_achievements
    age: number
    characterData:WvW_characters
    commander: boolean
    time: string
    user: string
    world: number
    wvw_rank: number
}

export interface WvW_Compacted extends WvW_achievements {
    id: string
    age: number
    characters:WvW_characters
    user: string
    wvw_rank: number
    date: string
}

export interface LoginRewards_Manager extends Manager{
    // no need for anythign else right now
}

export interface LoginRewardsItems {
    // MC
    68332: number;
    68318: number;
    68330: number;
    68333: number;
    // mc utself
    19976: number;

    // laurels
    68314: number;
    68339: number;
    68327: number;
    68336: number;
    68328: number;
    68334: number;
    68351: number;

    // Luck
    68319: number;
    68337: number;
    68335: number;
    68331: number;

    // chest of black lion goods
    68325: number;
    68340: number;
    68341: number;
    68315: number;

    // crafting mats
    68320: number;
    68322: number;
    // end chest options
    68350: number;
    68352: number;

    // Experience
    68329: number;
    68317: number;
    68338: number;
    68316: number;
    68354: number;
    // tombs of knowledge
    43766: number;

    // misc
    68321: number;
    68323: number;
    68324: number;

    // chest of loyalty
    68326: number;
}

export interface LoginRewards extends LoginRewardsItems {
    id: string;
    displayName: string;
    lastUpdate: string;

    wallet_laurel: number;
}

export interface Ctx {
    db: Db
    server:  restify.Server
}

export interface ResetPassword {
    user: string
    key: string
    expires: string
}

export interface Accounts_sessions {
    session: string
    firstAccessed: string
    ip: string
    lastAccessed: string
    logins: number
    user: string
    userAgent: string
    userAgentInitial: string
}