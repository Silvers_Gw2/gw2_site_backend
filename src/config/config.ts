import dotenv from 'dotenv'
const packageData = require('../../../package.json')

dotenv.config();

export const name: string = packageData.name;
export const version: string = packageData.version;
export const env: string = process.env.ENVIROMENT || "production";
export const port: number = parseInt(process.env.PORT, 10) || 84;

export const proxy = process.env.PROXY || "";
export const saltRounds = parseInt(process.env.SALT_ROUNDS, 10) || 5;
export const IDCharacters = process.env.ID_CHARACERS || "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_~";
export const gw2APIVersion = process.env.GW2_API_VERSION || "2019-04-26T00:00:00Z";
export const userTiers = processUserAccountTiers(process.env.USER_TIERS);

// accounts
export const patreon = {
  page: process.env.PATREON_PAGE || "",
  key: process.env.PATREON_KEY || ""
};
export const email = {
  host: process.env.EMAIL_HOST || "",
  user: process.env.EMAIL_USER || "",
  pass: process.env.EMAIL_PASS || "",
};

// database stuff
const database = process.env.DB;
const dbPort = process.env.DB_PORT || 27017;
const dbLocation = process.env.DB_LOCATION;
const dbUser = process.env.DB_USER;
const dbPass = process.env.DB_PASS;

let dbURL = 'mongodb://'+dbUser+':'+dbPass+'@'+dbLocation+':'+dbPort;
if(dbUser === "" || dbPass === "" ){
  dbURL = 'mongodb://'+dbLocation+':'+dbPort;
}

let split = dbLocation.split('.')
if (split.length > 1) {
  dbURL = 'mongodb+srv://' + dbUser + ':' + dbPass + '@' + dbLocation + '/admin?ssl=false'
}

export const db = {
  uri: dbURL,
  database: database
};


function processUserAccountTiers(input?: string): {[propName: string]: number}{
  if(typeof input ==="undefined"){
    return {"1":12,"2":6,"3":1,"4":0.25}
  }

  let result = {};

  let segments = input.split(",");
  for(let i=0; i<segments.length; i++){
    result[i+1] = parseFloat(segments[i]) || 12;
  }
  return result;
}