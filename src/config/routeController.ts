import {account, lists} from "../routes/website"
import {verification} from "../routes/verification"
import {gemstore} from "../routes/gemstore"
import {recovery} from "../routes/recovery"
import { transactions, wvwStats } from "../routes/transactions"
import {Ctx} from "./db_entities";
import {RouteLoginRewards, RouteFreeBagSlots} from "../routes/customEndpoints";

export const routes = (ctx: Ctx) => {
  transactions(ctx)
  account(ctx)
  lists(ctx)
  wvwStats(ctx)
  verification(ctx)
  recovery(ctx)
  gemstore(ctx)
  RouteLoginRewards(ctx);
  RouteFreeBagSlots(ctx);
}
