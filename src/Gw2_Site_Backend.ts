import { name,version, port, env } from "./config/config"
import restify from "restify"
import { liftDB } from "./config/db"
import { routes } from "./config/routeController"
import corsMiddleware from "restify-cors-middleware";

const server = restify.createServer({ name: name, version: version })

server.use(restify.plugins.bodyParser({ mapParams: true }))
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.fullResponse())

const cors = corsMiddleware({origins: ["*"], allowHeaders: ["*"], exposeHeaders: ["*"]})
server.pre(cors.preflight);
server.use(cors.actual);

server.listen(port, async () => {
  let {db} = await liftDB(true)

  routes({ db, server })

  // report success
  console.log(`[info]: ${name} ${version} ready to accept connections on port ${port} in ${env} environment.`)
})
