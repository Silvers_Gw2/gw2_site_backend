import axios from "axios";
import {gw2APIVersion, IDCharacters} from "../config/config"
import Proxy from "../classes/Proxy"
import { verifySession, beautifyJSON } from "../JS/functions"
import {Ctx, Account_Game} from "../config/db_entities";
import {getItems} from "../config/db";

const proxy = new Proxy()

export const verification = (ctx: Ctx) => {
  const { db, server } = ctx

  // require auth
  // Manage the verification lists for the user
  server.post('/gw2_site/v1/verification/management/:sorting', async (req, res, next) => {
    let sorting = req.params.sorting.toString() || ''
    let session = await verifySession(db, req.header('session'), req)
    if (session.result === 'error') {
      // session does not exist
      res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }
    let user = session.success
    let listData = req.body

    let userData = []
    let listsData = []
    let result = { result: 'success' }
    if (sorting === 'new') {
      let randoString = generateID(IDCharacters, 15)
      // adds to teh account
      let settingData = {}
      settingData['verification.' + randoString] = listData.name
      userData.push({
        updateOne: {
          'filter': { user: user },
          'update': { $set: settingData },
          'upsert': true
        }
      })

      // adds to the lists collection
      listsData.push({
        updateOne: {
          'filter': { id: randoString },
          'update': {
            $set: {
              id: randoString,
              user: user,
              gameID: listData.user,
              items: listData.items || [],
              currency: listData.currency || [],
              professions: listData.professions || []
            }
          },
          'upsert': true
        }
      })

      result["success"] = randoString
    } else {
      // verify they have access to the list, only fires if the new flag isnt active
      let userAccount = await db.collection('accounts').find({ user: user }).project({ _id: 0 }).toArray().catch((err) => {console.log(err);return []})
      if (
          userAccount.length === 0 ||
        typeof userAccount[0].verification === 'undefined' ||
        typeof userAccount[0].verification[listData.id] === 'undefined'
      ) {
        res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid access' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }
    }

    if (sorting === 'update') {
      // adds to the lists collection
      listsData.push({
        updateOne: {
          'filter': { id: listData.id },
          'update': {
            $set: {
              items: listData.items,
              currency: listData.currency,
              professions: listData.professions
            }
          },
          'upsert': true
        }
      })
      result["success"] = listData.id
    }

    if (sorting === 'delete') {
      // updating only the lists collection
      let settingData = {}
      settingData['verification.' + listData.id] = ''
      userData.push({
        updateOne: {
          'filter': { user: user },
          'update': { $unset: settingData },
          'upsert': true
        }
      })

      // adds to the lists collection
      listsData.push({
        deleteOne: {
          'filter': { id: listData.id }
        }
      })
      result["success"] = listData.id
    }

    if (userData.length > 0) {
      await db.collection('accounts').bulkWrite(userData, { ordered: false }).catch((err) => {console.log(err);return []})
    }
    if (listsData.length > 0) {
      await db.collection('verification').bulkWrite(listsData, { ordered: false }).catch((err) => {console.log(err);return []})
    }
    res.sendRaw(200, beautifyJSON(result, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })

    next()
  })

  // this gets specific requested list
  server.get('/gw2_site/v1/verification/management/lists', async (req, res, next) => {
    let session = await verifySession(db, req.header('session'), req)
    if (session.result === 'error') {
      // session does not exist
      res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    let list = await db.collection('verification').find({ id: req.header('listID') }).project({ _id: 0 }).toArray().catch((err) => {console.log(err);return []})

    if (list.length === 0) {
      // session does not exist
      res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid list' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    res.sendRaw(200, beautifyJSON({ result: 'success', success: list[0] }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    next()
  })

  // no auth required

  server.get('/gw2_site/v1/verification/:listID', async (req, res, next) => {
    let listID = req.params.listID.toString() || ''

    // see if an id is supplied
    if (listID === '') {
      // no list selected
      res.sendRaw(401, beautifyJSON({ result: 'error', error: 'No list selected' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    // see if the list exists
    let list = await db.collection('verification').find({ id: listID }).project({ _id: 0 }).toArray().catch((err) => {console.log(err);return []})

    if (list.length === 0) {
      // session does not exist
      res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid list' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    let user = list[0].user
    let gameAccount = list[0].gameID

    // then check if there is a cached result
    let cached = await db.collection('verification_cache').find({ id: gameAccount }).project({ _id: 0 }).toArray().catch((err) => {console.log(err);return []})
    let cachedFlag = false
    if (cached.length > 0) {
      // cached version exists
      // check if its a recent result
      if (new Date(cached[0].lastUpdate).getTime() > (new Date().getTime() - (60 * 60 * 1000))) {
        // if it is recent then use teh cached result
        cachedFlag = true
      }
      // if it is not then proceed as normal
    }

    let items, currency, lastUpdate, professions
    if (cachedFlag) {
      items = cached[0].items
      currency = cached[0].currency
      professions = cached[0].professions
      lastUpdate = cached[0].lastUpdate
    } else {
      // if no cached result lookup the account and get teh api key

      let userAccount = await db.collection('accounts').find({ user: user }).project({ _id: 0 }).toArray().catch((err) => {console.log(err);return []})
      if (userAccount.length === 0){
        res.sendRaw(401, beautifyJSON({ result: 'error', error: 'User does nto exist' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }

      let account_game_tmp = await getItems<Account_Game>(db, "accounts_game", {displayName: gameAccount});
      if (account_game_tmp.length === 0){
        res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Account not listed' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }
      let account_game = account_game_tmp[0];
      let apiKey = account_game.key;

      if (
        typeof userAccount[0].gameAccounts === 'undefined' ||
        userAccount[0].gameAccounts.indexOf(account_game.id) === -1
      ) {
        // then the character is not part of teh suers
        res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid access' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }

      // test the proxy
      await proxy.test();

      // then lookup teh values
      let itemDataRaw = await axios.get("https://api.datawars2.ie/gw2/v1/items/json?fields=id,binding&filter=id:gt:0").then(response => response.data).catch((err) => {console.log(err);return []}) as Array<object>
      let itemData = {}
      for (let i = 0; i < itemDataRaw.length; i++) {
        if (typeof itemDataRaw[i]["id"] === 'string') { continue }
        if (typeof itemDataRaw[i]["binding"] !== 'undefined') {
          let binding
          if (itemDataRaw[i]["binding"] === 'None') { binding = undefined }
          if (itemDataRaw[i]["binding"] === 'Account') { binding = 'Account' }
          if (itemDataRaw[i]["binding"] === 'Soul') { binding = 'Character' }
          itemData[itemDataRaw[i]["id"]] = binding
        }
      }

      items = {}
      let urls = [
        `https://api.guildwars2.com/v2/account/bank?access_token=${apiKey}&v=${gw2APIVersion}`,
        `https://api.guildwars2.com/v2/account/materials?access_token=${apiKey}&v=${gw2APIVersion}`,
        `https://api.guildwars2.com/v2/account/inventory?access_token=${apiKey}&v=${gw2APIVersion}`,
      ]

      let characters = await proxy.get(`https://api.guildwars2.com/v2/characters?access_token=${apiKey}&v=${gw2APIVersion}`).then(response => response.data).catch((err) => {console.log(err);return []}) as Array<object>

      for (let i = 0; i < characters.length; i++) {
        urls.push(`https://api.guildwars2.com/v2/characters/${characters[i]}/inventory?access_token=${apiKey}&v=${gw2APIVersion}`)
        urls.push(`https://api.guildwars2.com/v2/characters/${characters[i]}/equipment?access_token=${apiKey}&v=${gw2APIVersion}`)
      }

      let promises = urls.map( async(url) => await proxy.get(url).then(response => response.data).catch(err => console.log(err)))

      await Promise.all(promises)
        .then(
            (responses) => {
              responses.map(
                  (response) => {
                    items = processData(items, response, itemData)
                  })
            })
        .catch((err) => { console.log(err) })

      currency = {}
      let wallet = await proxy.get(`https://api.guildwars2.com/v2/account/wallet?access_token=${apiKey}&v=${gw2APIVersion}`).then(response => response.data).catch((err) => {console.log(err);return []}) as Array<object>
      for (let i = 0; i < wallet.length; i++) {
        currency[wallet[i]["id"]] = wallet[i]["value"]
      }

      urls = []
      for (let i = 0; i < characters.length; i++) {
        urls.push('https://api.guildwars2.com/v2/characters/' + characters[i] + '/crafting?access_token=' + apiKey + '&v=' + gw2APIVersion)
      }

      let professionsRaw = [{ 'id': 1, 'name': 'Armorsmith', 'img': 'https://render.guildwars2.com/file/2952B92FA93C03A5281F94D223A4CE4C7E0B0906/102461.png' }, { 'id': 2, 'name': 'Artificer', 'img': 'https://render.guildwars2.com/file/0D75999D6DEA1FDFF9DB43BBC2054B62764EB9A0/102463.png' }, { 'id': 3, 'name': 'Chef', 'img': 'https://render.guildwars2.com/file/424E410B90DE300CEB4A1DE2AB954A287C7A5419/102465.png' }, { 'id': 4, 'name': 'Huntsman', 'img': 'https://render.guildwars2.com/file/0C91017241F016EF35A2BCCE183CA9F7374023FC/102462.png' }, { 'id': 5, 'name': 'Jeweler', 'img': 'https://render.guildwars2.com/file/F97F4D212B1294052A196734C71BCE42E199735B/102458.png' }, { 'id': 6, 'name': 'Leatherworker', 'img': 'https://render.guildwars2.com/file/192D1D0D73BA7899F1745F32BAC1634C1B4671BF/102464.png' }, { 'id': 7, 'name': 'Scribe', 'img': 'https://render.guildwars2.com/file/F95DFA3FBDCC9E9F317551A903E5A2A6DF1CC7E3/1293677.png' }, { 'id': 8, 'name': 'Tailor', 'img': 'https://render.guildwars2.com/file/0EB64958BE48AB9605DD56807713215095B8BEED/102459.png' }, { 'id': 9, 'name': 'Weaponsmith', 'img': 'https://render.guildwars2.com/file/AEEF1CF774EE0D5917D5E1CF3AAC269FEE5EC03A/102460.png' }]
      let professionsData = {}
      for (let i = 0; i < professionsRaw.length; i++) {
        professionsData[professionsRaw[i].name] = professionsRaw[i]
      }

      professions = {}
      promises = urls.map( async(url) =>  await proxy.get(url).then(response => response.data).catch((err) => {console.log(err);return { 'crafting': [] }}))
      await Promise.all(promises)
        .then((responses) => {
          responses.map((response) => {
            let professionsArray = response.crafting
            for (let i = 0; i < professionsArray.length; i++) {
              if (typeof professions[professionsData[professionsArray[i].discipline].id] === 'undefined') {
                professions[professionsData[professionsArray[i].discipline].id] = 0
              }
              if (professionsArray[i].rating > professions[professionsData[professionsArray[i].discipline].id]) {
                professions[professionsData[professionsArray[i].discipline].id] = professionsArray[i].rating
              }
            }
          })
        })
          .catch((err) => {console.log(err);return []})

      lastUpdate = new Date().toISOString()
    }

    let result = {
      id: list[0].id,
      user: list[0].gameID,
      lastUpdate: lastUpdate,
      items: [],
      currency: [],
      professions: []
    }

    // then run through the list and see if what they have is valid
    for (let i = 0; i < list[0].items.length; i++) {
      let tmp = list[0].items[i]
      tmp.binding = { character: 0, account: 0, none: 0 }
      if (typeof items[list[0].items[i].id] !== 'undefined') {
        if (items[list[0].items[i].id].character >= list[0].items[i].qty) { tmp.binding.character = 1 }
        if (items[list[0].items[i].id].account >= list[0].items[i].qty) { tmp.binding.account = 1 }
        if (items[list[0].items[i].id].none >= list[0].items[i].qty) { tmp.binding.none = 1 }
      }
      result.items.push(tmp)
    }

    // for currences
    for (let i = 0; i < list[0].currency.length; i++) {
      let tmp = list[0].currency[i]
      tmp.valid = 0
      if (currency[list[0].currency[i].id] >= list[0].currency[i].qty) { tmp.valid = 1 }
      result.currency.push(tmp)
    }

    // for professions
    for (let i = 0; i < list[0].professions.length; i++) {
      let tmp = list[0].professions[i]
      tmp.valid = 0
      if (professions[list[0].professions[i].id] >= list[0].professions[i].qty) { tmp.valid = 1 }
      result.professions.push(tmp)
    }

    // send result off to user
    res.sendRaw(200, beautifyJSON({ result: 'success', success: result }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })

    // then store teh updated result in teh cache
    if (!cachedFlag) {
      let cacheData = {
        id: gameAccount,
        lastUpdate: lastUpdate,
        items: items,
        currency: currency,
        professions: professions
      }
      await db.collection('verification_cache').updateOne({ id: gameAccount }, { $set: cacheData }, { upsert: true }).catch((err) => {console.log(err);return []})
    }

    next()
  })
}

function generateID (imputCharacters, ID_LENGTH) {
  let rtn = ''
  for (let i = 0; i < ID_LENGTH; i++) {
    rtn += imputCharacters.charAt(Math.floor(Math.random() * imputCharacters.length))
  }
  return rtn
}

function processData (tmp, result, itemData) {
  if(typeof result === "undefined"){return tmp}
  if (typeof result.equipment !== 'undefined') {
    result = result.equipment
  }

  if (typeof result.bags !== 'undefined') {
    let tmp = []
    for (let i = 0; i < result.bags.length; i++) {
      if (result.bags[i] === null) { continue }
      for (let j = 0; j < result.bags[i].inventory.length; j++) {
        tmp.push(result.bags[i].inventory[j])
      }
    }
    result = tmp
  }

  for (let i = 0; i < result.length; i++) {
    if (result[i] === null) { continue }
    if (result[i].count === 0) { continue }
    if (typeof result[i].count === 'undefined') { result[i].count = 1 }

    if (typeof tmp[result[i].id] === 'undefined') {
      tmp[result[i].id] = { character: 0, account: 0, none: 0 }
    }
    if (typeof result[i].binding === 'undefined') {
      tmp[result[i].id].none += result[i].count
    } else {
      if (result[i].binding === 'Character') {
        tmp[result[i].id].character += result[i].count
      }
      if (result[i].binding === 'Account') {
        tmp[result[i].id].account += result[i].count
      }
    }
    if (typeof result[i].upgrades !== 'undefined') {
      for (let j = 0; j < result[i].upgrades.length; j++) {
        if (typeof tmp[result[i].upgrades[j]] === 'undefined') {
          tmp[result[i].upgrades[j]] = { character: 0, account: 0, none: 0 }
        }

        let binding = itemData[result[i].upgrades[j]]
        if (typeof binding === 'undefined') {
          tmp[result[i].upgrades[j]].none += 1
        } else {
          if (binding === 'Character') {
            tmp[result[i].upgrades[j]].character += 1
          }
          if (binding === 'Account') {
            tmp[result[i].upgrades[j]].account += 1
          }
        }
      }
    }

    if (typeof result[i].infusions !== 'undefined') {
      for (let j = 0; j < result[i].infusions.length; j++) {
        if (typeof tmp[result[i].infusions[j]] === 'undefined') {
          tmp[result[i].infusions[j]] = { character: 0, account: 0, none: 0 }
        }

        let binding = itemData[result[i].infusions[j]]
        if (typeof binding === 'undefined') {
          tmp[result[i].infusions[j]].none += 1
        } else {
          if (binding === 'Character') {
            tmp[result[i].infusions[j]].character += 1
          }
          if (binding === 'Account') {
            tmp[result[i].infusions[j]].account += 1
          }
        }
      }
    }
  }
  return tmp
}
