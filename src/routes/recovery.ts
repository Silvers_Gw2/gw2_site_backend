import bcrypt from "bcryptjs"
import nodemailer from "nodemailer"

import { deleteItems, getItems, addItem, Db } from "../config/db"
import { email as Email, IDCharacters, saltRounds} from "../config/config"
import { dateAgo } from "../JS/functions"
import { Account, Ctx, ResetPassword } from "../config/db_entities";

let transporter = nodemailer.createTransport({ pool: true, host: Email.host, port: 465, secure: true, auth: { user: Email.user, pass: Email.pass }, tls: {rejectUnauthorized: false} })

export const recovery = (ctx: Ctx) => {
  const { db, server } = ctx

  // /recovery/account
  // this looks up the account assossiated with the email entered, sends accountname to said email
  server.post('/gw2_site/v1/recovery/account', async (req, res, next) => {
    // no indication is given regarding if an account exists or not

    let email = req.header('email')
    let account = await checkEmail(db, email, "account")
    if (account === null) { res.send(200); return }

    // setting this first
    let tmp: Account["emailDetails"]
    if (typeof account.emailDetails === 'undefined') {
      tmp = {
        account: undefined,
        password: undefined
      }
    } else {
      tmp = account.emailDetails
    }
    tmp.account = new Date().toISOString()

    await addItem<Account>(db, "accounts", { user: account.user, emailDetails: tmp },"user").catch(err => console.log(err))

    let message = {
      from: {
        name: 'DataWars2',
        address: 'recovery@datawars2.ie'
      },
      to: email,
      subject: 'DataWars2 - Account Reminder',
      text: 'Hi there! \n' +
        ' \n' +
        'Your DataWars2 username is: ' + account.user + '\n' +
        'Have a good day. \n' +
        ' \n' +
        'DataWars2 \n' +
        ' \n' +
        ' \n' +
        'This address (recovery@datawars2.ie) does not support replies, please use support@datawars2.ie for support/queries.',
      html: '<div><h4>Hi there!</h4><p>Your DataWars2 username is: ' + account.user + ' <br />Have a good day.</p><p>DataWars2 <br /></p><hr />This address (recovery@datawars2.ie) does not support replies, please use support@datawars2.ie for support/queries.</div>'
    }

    await transporter.sendMail(message).catch(err => console.log(err))

    res.send(200)
    next()
  })

  // /recovery/password
  // same as above, but also generates link to reset password.
  server.post('/gw2_site/v1/recovery/password', async (req, res, next) => {
    let email = req.header('email')
    let account = await checkEmail(db, email, "password")
    if (account === null) { res.send(200); return }
    // send mail past this point

    let tmp: Account["emailDetails"]
    if (typeof account.emailDetails === 'undefined') {
      tmp = {
        account: undefined,
        password: undefined
      }
    } else {
      tmp = account.emailDetails
    }
    tmp.password = new Date().toISOString()

    await addItem<Account>(db, "accounts", { user: account.user, emailDetails: tmp },"user").catch(err => console.log(err))

    let key = generateID(IDCharacters, 129)
    let tempReset: ResetPassword = {
      user: account.user,
      key: key,
      expires: dateAgo('minutes', -60)
    }

    // add new key
    // only one reset per use at a time
    await addItem<ResetPassword>(db, "resetPassword", tempReset,"user").catch(err => console.log(err))

    // create email next, also include the link
    let message = {
      from: {
        name: 'DataWars2',
        address: 'recovery@datawars2.ie'
      },
      to: email,
      subject: 'DataWars2 - Password Reset',
      text: 'Hi there!' +
        ' \n' +
        ' \n' +
        'A password reset has been requested for ' + account.user +
        ' \n' +
        'If you requested it please head to https://datawars2.ie/#/reset/password?auth=' + key +
        ' \n' +
        'If you did not request it then either someone is trying to use DataWars2 to confirm the email account exists or they have access to your email. Either way please check your email security.' +
        ' \n' +
        ' \n' +
        'DataWars2' +
        ' \n' +
        ' \n' +
        'This address (recovery@datawars2.ie) does not support replies, please use support@datawars2.ie for support/queries.',
      html: '<div>' +
        '<h4>' +
          'Hi there!' +
        '</h4>' +
        '<p>' +
          'A password reset has been requested for ' + account.user + ' ' +
          '<br />' +
          'If you requested it please head to https://datawars2.ie/#/reset/password?auth=' + key + '' +
          '<br />' +
          'If you did not request it then either someone is trying to use DataWars2 to confirm the email account exists or they have access to your email. Either way please check your email security.' +
        '</p>' +
        '<p>' +
          'DataWars2' +
          '<br />' +
        '</p>' +
        '<hr />' +
        'This address (recovery@datawars2.ie) does not support replies, please use support@datawars2.ie for support/queries.' +
      '</div>'
    }

    await transporter.sendMail(message).catch(err => console.log(err))

    res.send(200)
    next()
  })

  // /recovery/password/:changeID
  // takes the ID and looks it up
  // then if its valid
  server.post('/gw2_site/v1/recovery/password/reset', async (req, res, next) => {
    let key = req.header('key')
    let newPassword = req.header('newPassword')

    if (key === null) { res.send(200); return }
    if (newPassword === null) { res.send(200); return }

    if (typeof key === 'undefined') { res.send(200); return }
    if (typeof newPassword === 'undefined') { res.send(200); return }

    // clear old entries
    await deleteItems(db, 'resetPassword', { expires: { $lt: new Date().toISOString() } }).catch(err => console.log(err))

    // find the key
    //let verification = await db.collection('resetPassword').find({ key: key }).toArray().catch(err => console.log(err))
    let verification = await getItems<ResetPassword>(db, "resetPassword", { key: key })
    if (verification.length === 0) { res.send(200); return }

    let account = verification[0]
    let hashed = await bcrypt.hash(newPassword, saltRounds)

    await addItem<Account>(db, "accounts", { user: account.user, password: hashed },"user").catch(err => console.log(err))

    // clear used key
    await deleteItems(db, 'resetPassword', { key: key }).catch(err => console.log(err))

    let emailRaw = await getItems<Account>(db, "accounts", { user: account.user }, { _id: 0, email: 1 })

    let email = emailRaw[0].email
    // send comfirmation of password change
    let message = {
      from: {
        name: 'DataWars2',
        address: 'recovery@datawars2.ie'
      },
      to: email,
      subject: 'DataWars2 - Password Reset Complete',
      text: 'Hi there!' +
        ' \n' +
        ' \n' +
        'A password reset has been completed for ' + account.user +
        ' \n' +
        ' \n' +
        'DataWars2' +
        ' \n' +
        ' \n' +
        'This address (recovery@datawars2.ie) does not support replies, please use support@datawars2.ie for support/queries.',
      html: '<div>' +
        '<h4>' +
        'Hi there!' +
        '</h4>' +
        '<p>' +
        'A password reset has been completed for ' + account.user + ' ' +
        '<br />' +
        '</p>' +
        '<p>' +
        'DataWars2' +
        '<br />' +
        '</p>' +
        '<hr />' +
        'This address (recovery@datawars2.ie) does not support replies, please use support@datawars2.ie for support/queries.' +
        '</div>'
    }

    await transporter.sendMail(message).catch(err => console.log(err))

    res.send(200)
    next()
  })
}

async function checkEmail (db: Db, email: string, type: string): Promise<null | Account> {
  if (typeof email === 'undefined') { return null }

  let verification = await getItems<Account>(db, "accounts", { email: email }, { _id: 0, emailDetails: 1, user: 1 })
  if (verification.length !== 1) { return null }

  let account = verification[0]
  if (account.user === null) { return null }
  if (typeof account.user === 'undefined') { return null }
  if (typeof account.emailDetails !== 'undefined') {
    // check the date of the last email
    if (account.emailDetails[type] >= dateAgo('minutes', 60)) { return null }
  }
  return account
}

function generateID (imputCharacters, ID_LENGTH) {
  let rtn = ''
  for (let i = 0; i < ID_LENGTH; i++) {
    rtn += imputCharacters.charAt(Math.floor(Math.random() * imputCharacters.length))
  }
  return rtn
}
