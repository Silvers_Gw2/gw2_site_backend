import bcrypt from "bcryptjs"
import { getItems,addItem, Db } from "../config/db"
import {Account_Game, Account, Transaction_Manager, WvW_Manager, Ctx, Account_base} from "../config/db_entities";
import { verifySession, beautifyJSON, createUniqueSession} from "../JS/functions"
import { saltRounds} from "../config/config"
import Proxy from "../classes/Proxy"

const proxy = new Proxy()

export const account = (ctx: Ctx) => {
  const { db, server } = ctx

  server.get('/gw2_site/v1/website/login', async (req, res, next) => {
    let user = req.header('user')
    let password = req.header('password')

    let verification = await getItems<Account>(db, "accounts", { user: user }, { _id: 0, password: 1 })

    if (verification.length === 0) {
      if (typeof password === 'undefined') {
        res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Please set a password' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }
      let email = req.header('email')
      if (typeof email === 'undefined') {
        res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Please set an email' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }
      let existingEmails = await getItems<Account>(db, "accounts", { email: email }, { _id: 0, email: 1 })
      if (existingEmails.length > 0) {
        res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Email in use' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }
      // this username does not exist

      let tmp: Account = {
        user:user,
        firstAdded: new Date().toISOString(),

        password: await bcrypt.hash(password, saltRounds),
        email:email,
        emailDetails: {account: new Date(1).toISOString(), password: new Date(1).toISOString()},

        accountLevel:1,
        donation: { private: 0, patreon: 0, custom: 0 },

        gameAccounts: [],

        lists: {},
        verification: {},
      }

      let result = await addItem<Account>(db, "accounts", tmp, "user").catch((err) => {console.log(err);return "err"})
      if (result === 'err') {
        return res.send(200, { result: 'error', error: 'Contact Silver'} )
      }
    }
    if (verification.length === 1) {
      const match = await bcrypt.compare(password, verification[0].password)
      if (!match) {
        res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Invalid credentials' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }
    }

    let session = await createUniqueSession(db, user, req)
    res.sendRaw(200, beautifyJSON({ result: 'success', session: session }, 'min'), { 'Content-Type': 'application/json; charset=utf-8' })
    next()
  })

  server.get('/gw2_site/v1/website/account', async (req, res, next) => {
    let session = await verifySession(db, req.header('session'), req)
    if (session.result === 'error') {
      // session does not exist
      res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }
    let user = session.success

    let account_tmp = await getItems<Account>(db, "accounts", { user: user }, { _id: 0, password: 0, firstAdded: 0 })
    if (account_tmp.length !== 1) {
      // user does not exist, or somehow multiple users
      res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Invalid user' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    let account = await getOldGameAccounts(db, account_tmp[0])
    res.sendRaw(200, beautifyJSON({ result: 'success', account: account }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    next()
  })

  server.post('/gw2_site/v1/website/account/:sorting', async (req, res, next) => {
    let sorting = req.params.sorting.toString()

    let session = await verifySession(db, req.header('session'), req)
    if (session.result === 'error') {
      // session does not exist
      res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }
    let user = session.success

    let verification = await getItems<Account>(db, "accounts", { user: user }, { _id: 0, password: 0, firstAdded: 0 })
    if (verification.length === 0 || verification.length > 1) {
      // user does not exist, or somehow multiple users
      res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Invalid user' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    let account = verification[0]
    // tmp is the new account
    let tmp: Partial<Account> = {}
    if (sorting === 'email') {
      let email = req.header('email')
      if (typeof email !== 'undefined' && email !== null) {
        let existingEmails = await getItems<Account>(db, "accounts", { email: email }, { _id: 0, email: 1 })
        if (existingEmails.length === 0) {
          tmp.email = email
        }
      }
    }
    if (sorting === 'password') {
      tmp.password = await bcrypt.hash(req.header('password'), saltRounds)
    }
    if (sorting === 'apiKey') {
      // test the proxy
      await proxy.test();

      let key = req.header('apiKey')

      let permissions = await proxy.get(`https://api.guildwars2.com/v2/tokeninfo?access_token=${key}`).catch(err => {console.log(err); return {status:500, error:err}})
      if (permissions.status !== 200) {
        res.sendRaw(200, beautifyJSON({ result: 'error', error: permissions.error }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }

      if (permissions.data.permissions.length < 10) {
        res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Missing ' + (10 - permissions.data.permissions.length) + ' permission' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }

      let gw2Account = await proxy.get(`https://api.guildwars2.com/v2/account?access_token=${key}`).catch(err => {console.log(err); return {status:500, error:err}})

      if (gw2Account.status !== 200) {
        res.sendRaw(200, beautifyJSON({ error: gw2Account.error }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }

      await addGameAccount (db, account, gw2Account, key, permissions.data.permissions )
      await addToTransactionManager (db, gw2Account.data.id)
      await addToWvWnManager (db, gw2Account.data.id)

      tmp.gameAccounts = account.gameAccounts
      // check if id already exists
      if(tmp.gameAccounts.indexOf(gw2Account.data.id) === -1){
        tmp.gameAccounts.push(gw2Account.data.id)
      }
    }

    if (sorting === 'deleteKey') {
      // may need to return to this in teh future

      let deletingKey = req.header('apiKey')

      // find the key
      let game_account_tmp = await getItems<Account_Game>(db, "accounts_game", { key: deletingKey }, {})

      // check if it exists
      if(game_account_tmp.length === 0){
        res.sendRaw(200, beautifyJSON({ error: "Account with this key does not exist" }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }

      let game_account = game_account_tmp[0]

      // check if the user has it
      if(account.gameAccounts.indexOf(game_account.id) === -1){
        // user does not ave the key on their account

        res.sendRaw(200, beautifyJSON({ error: "key is not on account" }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
        return
      }

      // just remove it from the array
      // the controller_accounts will mop up later
      tmp.gameAccounts = account.gameAccounts.filter(item => item !== game_account.id)
    }

    if (Object.keys(tmp).length > 0) {
      let result = await db.collection('accounts').updateOne({ user: user }, { $set: tmp }, { upsert: true })
        .catch(async (err) => {
          console.log(err)
          res.send(500, 'Contact Silver')
          return 'err'
        })
      if (result === 'err') { return }
    }
    res.sendRaw(200, beautifyJSON({ result: 'success' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })

    next()
  })
}

export const lists = (ctx: Ctx) => {
  const { db, server } = ctx

  server.post('/gw2_site/v1/website/lists/:sorting', async (req, res, next) => {
    let sorting = req.params.sorting.toString() || ''
    let session = await verifySession(db, req.header('session'), req)
    if (session.result === 'error') {
      // session does not exist
      res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }
    let user = session.success
    let listData = req.body

    let userData = []
    let listsData = []
    let result = { result: 'success' }
    if (sorting === 'new') {
      let randoString = [...Array(145)].map(() => (~~(Math.random() * 36)).toString(36)).join('')
      // adds to teh account
      let settingData = {}
      settingData['lists.' + randoString] = listData.name
      userData.push({
        updateOne: {
          'filter': { user: user },
          'update': { $set: settingData },
          'upsert': true
        }
      })

      // adds to the lists collection
      listsData.push({
        updateOne: {
          'filter': { id: randoString },
          'update': { $set: { id: randoString, items: listData.items } },
          'upsert': true
        }
      })

      result["success"] = randoString
    }
    if (sorting === 'update') {
      // updating only the lists collection
      let settingData = {}
      settingData['lists.' + listData.id] = listData.name
      userData.push({
        updateOne: {
          'filter': { user: user },
          'update': { $set: settingData },
          'upsert': true
        }
      })

      // adds to the lists collection
      listsData.push({
        updateOne: {
          'filter': { id: listData.id },
          'update': { $set: { id: listData.id, items: listData.items } },
          'upsert': true
        }
      })
      result["success"] = listData.id
    }

    if (sorting === 'delete') {
      // updating only the lists collection
      let settingData = {}
      settingData['lists.' + listData.id] = ''
      userData.push({
        updateOne: {
          'filter': { user: user },
          'update': { $unset: settingData },
          'upsert': true
        }
      })

      // adds to the lists collection
      listsData.push({
        deleteOne: {
          'filter': { id: listData.id }
        }
      })
      result["success"] = listData.id
    }

    if (userData.length > 0) {
      await db.collection('accounts').bulkWrite(userData, { ordered: false }).catch((err) => {console.log(err)})
    }
    if (listsData.length > 0) {
      await db.collection('lists').bulkWrite(listsData, { ordered: false }).catch((err) => {console.log(err)})
    }
    res.sendRaw(200, beautifyJSON(result, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    next()
  })

  server.get('/gw2_site/v1/website/lists', async (req, res, next) => {
    let session = await verifySession(db, req.header('session'), req)
    if (session.result === 'error') {
      // session does not exist
      res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    let list = await db.collection('lists').find({ id: req.header('listID') }).project({ _id: 0 }).toArray().catch(err => res.send(500, err))

    if (list.length === 0) {
      // session does not exist
      res.sendRaw(200, beautifyJSON({ result: 'error', error: 'Invalid list' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    res.sendRaw(200, beautifyJSON({ result: 'success', success: list[0] }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    next()
  })
}

async function addGameAccount (db: Db, account:Account, gw2Account, key: string, permissions: string[] ) {
  let account_game_set: Partial<Account_Game> = {
    id: gw2Account.data.id,
    displayName: gw2Account.data.name,
    key: key,
    level: account.accountLevel,
    access: gw2Account.data.access,
    permissions: permissions,
    removed: false
  }
  let account_game_setOnInsert: Partial<Account_Game> = {
    firstAdded: new Date().toISOString(),
    created: gw2Account.data.created,
  }
  await db.collection('accounts_game').updateOne({ id: gw2Account.data.id }, { $set: account_game_set, $setOnInsert: account_game_setOnInsert }, { upsert: true }).catch((err) => {console.log(err)})

}

async function addToTransactionManager (db: Db, id: string) {
  let transaction_set: Partial<Transaction_Manager> = {
    active: 0,
    errorCount: 0,
  }
  let transaction_setOnInsert: Partial<Transaction_Manager> = {
    id: id,
    firstAdded: new Date().toISOString(),
    nextUpdate: new Date(1).toISOString(),
    lastUpdate: new Date(1).toISOString(),
    //active: 0,
    firstRun: 0,
    lastBuyID: 0,
    lastSellID: 0,
    //errorCount: 0,
    initialPages: {buy: 0, sell: 0},
  }
  await db.collection('transaction_manager').updateOne({ id: id }, { $set: transaction_set, $setOnInsert: transaction_setOnInsert }, { upsert: true }).catch((err) => {console.log(err)})

}

async function addToWvWnManager (db: Db, id: string) {
  let wvw_set: Partial<WvW_Manager> = {
    active: 0,
    errorCount: 0,
  }
  let wvw_setOnInsert: Partial<WvW_Manager> = {
    id: id,
    firstAdded: new Date().toISOString(),
    nextUpdate: new Date(1).toISOString(),
    lastUpdate: new Date(1).toISOString(),
    age: 0,
    characters: {},
    lastData: {
      world: 0,
      commander: false,
      wvw_rank: 0,
      achievements: {
        kills:0,
        yakDefend:0,
        yakCapture:0,
        campCapture:0,
        stoneMistCapture:0,
        towerCapture:0,
        keepCapture:0,
        objectiveCapture:0,
        supplySpentRepair:0,
        campDefend:0,
        stoneMistDefend:0,
        keepDefend:0,
        objectiveDefend:0,
        towerDefend:0,
      }
    },
  }
  await db.collection('wvw_manager').updateOne({ id: id }, { $set: wvw_set, $setOnInsert: wvw_setOnInsert }, { upsert: true }).catch((err) => {console.log(err)})
}

interface Account_GameAccount_old extends Account_base  {
  gameAccounts: {
    [propName: string]: {
      key: string
      permissions: string[]
    }
  }
}
async function getOldGameAccounts(db:Db, newAccount: Account): Promise<Partial<Account_GameAccount_old>>{
  let { accountLevel, donation, email, emailDetails, lists, user, verification, gameAccounts} = newAccount
  let tmp = { accountLevel, donation, email, emailDetails, lists, user, verification, gameAccounts: {} }

  let promises = []

  gameAccounts.forEach(id => {
    promises.push(getItems<Account_Game>(db, "accounts_game", {id: id}))
  })

   await Promise.all(promises)
       .then((results: Account_Game[][]) => {
         for(let i=0;i< results.length;i++){
           if(results[i].length !== 1){continue}
           let result = results[i][0]
           tmp.gameAccounts[result.displayName] = {
             key: result.key,
             permissions: result.permissions
           }
         }


       })
       .catch(err => console.log(err))

  return tmp
}