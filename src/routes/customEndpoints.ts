import { parse } from 'json2csv'
import {beautifyJSON, getLoginRewards, verifySession} from "../JS/functions"
import { Db, getItems } from "../config/db";
import {
  Account, Account_Game,
  Ctx,
  LoginRewards,
  LoginRewards_Manager, LoginRewardsItems
} from "../config/db_entities";
import restify from "restify";
import Proxy from "../classes/Proxy";



export const RouteLoginRewards = (ctx: Ctx) => {
  const { db, server } = ctx

  // json
  server.get('/gw2_site/v1/login_rewards', async (req, res, next) => await handleRequestsLoginRewards(req, res, next, db) )

  // csv
  server.get('/gw2_site/v1/login_rewards/csv', async (req, res, next) => await handleRequestsLoginRewards(req, res, next, db, true) )
}

export const RouteFreeBagSlots = (ctx: Ctx) => {
  const { db, server } = ctx

  // json
  server.get('/gw2_site/v1/free_bag_slots', async (req, res, next) => await handleRequestsFreeBagSlots(req, res, next, db) )

  // csv
  server.get('/gw2_site/v1/free_bag_slots/csv', async (req, res, next) => await handleRequestsFreeBagSlots(req, res, next, db, true) )
}

async function handleRequestsLoginRewards (req: restify.Request, res: restify.Response, next: restify.Next, db:Db, csv: boolean = false) {
  let account_response = await verifyAccount(req, db);

  if(!account_response.account && account_response.message){
    return res.sendRaw(account_response.status, beautifyJSON({ result: 'error', error: account_response.message }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
  }

  let laurels = await getLoginRewardsLocal(account_response.account, db);

  // 0 implies that there is nothing there
  if(laurels.length < account_response.account.gameAccounts.length){
    let accounts_old = laurels.map(entry => entry.id);

    const proxy = new Proxy()
    await proxy.test()

    for(let game_account of account_response.account.gameAccounts){
      if(accounts_old.indexOf(game_account) != -1){
        continue;
      }

      let account: LoginRewards_Manager = {
        active: 0,
        errorCount: 0,
        id: game_account,
        lastUpdate: new Date(0).toISOString(),
        nextUpdate: new Date(0).toISOString()
      }

      await getLoginRewards(account, db, true, proxy)
    }

    // grab the total array of laurels
    laurels = await getLoginRewardsLocal(account_response.account, db);
  }

  if(csv){
    let fields = ["id","displayName", "lastUpdate", "wallet_laurel" ];
    let bags: LoginRewardsItems = {19976: 0, 43766: 0, 68314: 0, 68315: 0, 68316: 0, 68317: 0, 68318: 0, 68319: 0, 68320: 0, 68321: 0, 68322: 0, 68323: 0, 68324: 0, 68325: 0, 68327: 0, 68328: 0, 68329: 0, 68330: 0, 68331: 0, 68332: 0, 68333: 0, 68334: 0, 68335: 0, 68336: 0, 68337: 0, 68338: 0, 68339: 0, 68340: 0, 68341: 0, 68350: 0, 68351: 0, 68352: 0, 68354: 0, 68326: 0};
    let keys = Object.keys(bags);
    fields.push(...keys)
    let csv_data = parse(laurels, { fields: fields })
    const filename = `api-datawars2-ie${req.url.split("?")[0].replace(/\//gi, '_').replace("_csv", '.csv')}`

    return res.send(200, csv_data, {
      'Content-Type': 'text/csv; charset=utf-8',
      'Content-disposition': `attachment; filename=${filename}`
    });
  } else {
    return res.sendRaw(200, beautifyJSON(laurels, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
  }
}



interface VerifyAccount {
  status: number;
  message?: string;
  account?: Account;
}

async function verifyAccount (req: restify.Request, db:Db): Promise<VerifyAccount> {
  let sessionKey = req.header('session') || req.query.session

  if (typeof sessionKey === 'undefined') {
    return {
      status: 500,
      message: 'no session parameter'
    }
  }

  let session = await verifySession(db, sessionKey, req)
  if (session.result === 'error') {
    // session does not exist

    return {
      status: 401,
      message: 'Invalid session'
    }
  }

  let verification = await getItems<Account>(db, "accounts", { user: session.success })
  // Checks if user exists
  if (verification.length === 0 || verification.length > 1) {
    // user does not exist, or somehow multiple users
    return {
      status: 401,
      message: 'Invalid session'
    }
  }

  return {
    status: 200,
    account: verification[0]
  }
}

async function getLoginRewardsLocal(account: Account, db:Db):Promise<Partial<LoginRewards>[]>{
  let laurels:Partial<LoginRewards>[] = [];
  for(let game_account of account.gameAccounts){
    let laurels_character: LoginRewards[] = await getItems<LoginRewards>(db, "LoginRewards", { id: game_account }).catch((err) => {console.log(err); return []})
    laurels.push(...laurels_character)
  }
  return laurels;
}

async function handleRequestsFreeBagSlots (req: restify.Request, res: restify.Response, next: restify.Next, db:Db, csv: boolean = false) {
  let account_response = await verifyAccount(req, db);

  if(!account_response.account && account_response.message){
    return res.sendRaw(account_response.status, beautifyJSON({ result: 'error', error: account_response.message }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
  }

  const proxy = new Proxy()
  await proxy.test()

  let results = [];
  for(let game_account_id of account_response.account.gameAccounts){
    let game_accounts: Account_Game[] = await getItems<Account_Game>(db, "accounts_game", { id: game_account_id }).catch((err) => {console.log(err); return []})
    if(game_accounts.length === 0){continue}
    let game_account = game_accounts[0];

    // get characters
    let characters: string[]  = await proxy.get(`https://api.guildwars2.com/v2/characters?access_token=${game_account.key}`).then((result) => result.data).catch(err => {console.log(err);return []})

    // mostly yoinked from teh frontend
    let requests = characters.map(async(character) => {
      let url = `https://api.guildwars2.com/v2/characters/${character}/inventory?access_token=${game_account.key}`
      return await proxy.get(url)
        .then((result) => {
          let json = result.data
          json.character = character
          return json
        })
        .catch((err) => {
        console.log(this.props.url, err.toString());
        return {
          character:character,
          bags: []
        }
      })
    })

    await Promise.all(requests)
      .then(responses =>{
        // loop through teh responses
        for(let i=0;i<responses.length;i++){

          let total = 0
          let filled = 0
          let characterBags = responses[i]
          if(typeof characterBags.bags === "undefined"){
            continue
          }
          for(let j=0;j<characterBags.bags.length;j++){
            if(characterBags.bags[j] === null){continue}
            total += characterBags.bags[j].size

            for(let k=0;k<characterBags.bags[j].inventory.length;k++){
              if(characterBags.bags[j].inventory[k] !== null){
                filled += 1
              }
            }
          }
          results.push({ account: game_account.displayName, name:characterBags.character, total:total, used:filled, free:total-filled})
        }
      })
      .catch(err => console.log(err))
  }

  if(csv){
    let csv_data = parse(results, { fields: ["account","name", "total", "used", "free" ] })
    const filename = `api-datawars2-ie${req.url.split("?")[0].replace(/\//gi, '_').replace("_csv", '.csv')}`

    return res.send(200, csv_data, {
      'Content-Type': 'text/csv; charset=utf-8',
      'Content-disposition': `attachment; filename=${filename}`
    });
  } else {
    return res.sendRaw(200, beautifyJSON(results, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
  }
}