import { verifySession, beautifyJSON} from "../JS/functions"
import {Ctx} from "../config/db_entities";

export const gemstore = (ctx: Ctx) => {
  const { db, server } = ctx

  server.post('/gw2_site/v1/lists/gemstore', async (req, res, next) => {
    let session = await verifySession(db, req.header('session'), req)
    if (session.result === 'error') {
      // session does not exist
      res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    // each user only gets one list
    // updating will overwrite it
    let listsData = [
      {
        updateOne: {
          'filter': { user: session.success },
          'update': {
            $set: {
              items: req.body.items || []
            },
            $setOnInsert: {
              user: session.success,
              firstAdded: new Date().toISOString()
            }
          },
          'upsert': true
        }
      }

    ]

    await db.collection('gemStoreLists').bulkWrite(listsData, { ordered: false }).catch((err) => {console.log(err)})

    res.sendRaw(200, beautifyJSON({ result: 'success' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })

    next()
  })

  // this gets specific requested list
  server.get('/gw2_site/v1/lists/gemstore', async (req, res, next) => {
    let session = await verifySession(db, req.header('session'), req)
    if (session.result === 'error') {
      // session does not exist
      res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
      return
    }

    let list = await db.collection('gemStoreLists').find({ user: session.success }).project({ _id: 0 }).toArray().catch((err) => {console.log(err);return []})

    if (list.length === 0) {
      // returns a fake list in case of first timers
      list = [{
        user: session.success,
        items: []
      }]
    }

    res.sendRaw(200, beautifyJSON({ result: 'success', success: list[0] }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })

    next()
  })
}
