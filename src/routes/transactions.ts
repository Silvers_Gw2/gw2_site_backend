import { parse } from 'json2csv'
import { beautifyJSON, verifySession } from "../JS/functions"
import { Db, getItems } from "../config/db";
import {Account_Game, Account, Transaction_Compacted, Manager, Ctx} from "../config/db_entities";

interface CSVConverter {
  ( history: Transaction_Compacted[], decimal: string): any
}
interface DBConfig {
  source: string
  manager: string
  csv?: CSVConverter
}

export const transactions = (ctx: Ctx) => {
  const { db, server } = ctx

  let buy: DBConfig = {
    source: "transaction_buy_compacted",
    manager: "transaction_manager",
    csv: convert_csv_transactions
  }

  let sell: DBConfig = {
    source: "transaction_sell_compacted",
    manager: "transaction_manager",
    csv: convert_csv_transactions
  }

  // json
  server.get('/gw2_site/v1/transactions/buy', async (req, res, next) => await handleTransactions(req, res, next, db, buy) )
  server.get('/gw2_site/v1/transactions/sell', async (req, res, next) => await handleTransactions(req, res, next, db, sell) )

  // csv
  server.get('/gw2_site/v1/transactions/buy/csv', async (req, res, next) => await handleTransactions(req, res, next, db,  buy, "csv") )
  server.get('/gw2_site/v1/transactions/sell/csv', async (req, res, next) => await handleTransactions(req, res, next, db,  sell, "csv") )
}

export const wvwStats = (ctx: Ctx) => {
  const { db, server } = ctx

  let wvw: DBConfig = {
    source: "wvw_compacted",
    manager: "wvw_manager"
  }
  server.get('/gw2_site/v1/wvwStats', async (req, res, next) => await handleTransactions(req, res, next, db, wvw) )
}

async function handleTransactions (req, res, next, db:Db,  config: DBConfig, type: string = "json") {
  let beautify = req.query.beautify || 'human'

  let sessionKey = req.header('session') || req.query.session
  let decimal = req.query.decimal || '.'
  let accountID = req.header('accountID') || req.query.accountID

  if (typeof sessionKey === 'undefined') { res.send(500, 'no session parameter'); return }
  if (typeof accountID === 'undefined') { res.send(500, 'no accountID parameter'); return }

  let session = await verifySession(db, sessionKey, req)
  if (session.result === 'error') {
    // session does not exist
    res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid session' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    return
  }
  let user = session.success

  let verification = await getItems<Account>(db, "accounts", { user: user })
  // Checks if user exists
  if (verification.length === 0 || verification.length > 1) {
    // user does not exist, or somehow multiple users
    res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid user' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    return
  }
  let account = verification[0]

  let game_account = await getItems<Account_Game>(db, "accounts_game", { displayName: accountID })
  if (game_account.length === 0) {
    // user does not exist, or somehow multiple users
    res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid Game Account' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    return
  }

  // checks if they have access to the game account
  if (account.gameAccounts.indexOf(game_account[0].id) === -1) {
    res.sendRaw(401, beautifyJSON({ result: 'error', error: 'No access to this account' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    return
  }

  let zeroed = new Date(new Date().setUTCHours(0, 0, 0, 0))
  let sixMonths:any = new Date().setDate(zeroed.getDate() - 183)
  sixMonths = new Date(sixMonths).toISOString()
  let twelveMonths:any = new Date().setDate(zeroed.getDate() - 365)
  twelveMonths = new Date(twelveMonths).toISOString()

  let find
  if (account.accountLevel >= 3) {
    find = { user: game_account[0].id }
  }
  if (account.accountLevel === 2) {
    find = { user: game_account[0].id, date: { $gte: twelveMonths } }
  }
  if (account.accountLevel === 1) {
    find = { user: game_account[0].id, date: { $gte: sixMonths } }
  }

  let historyAccount = await getItems<Manager>(db, config.manager, { id: game_account[0].id })
  if (historyAccount.length === 0 || historyAccount.length > 1) {
    // user does not exist, or somehow multiple users
    res.sendRaw(401, beautifyJSON({ result: 'error', error: 'Invalid user' }, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })
    return
  }

  let history = await getItems<Transaction_Compacted>(db, config.source, find, { _id: 0, id: 0 })

  if(type === "csv" && config.csv){
    let csv = config.csv(history, decimal)
    const filename = `api-datawars2-ie${req.url.split("?")[0].replace(/\//gi, '_').replace("_csv", '.csv')}`
    res.send(200, csv, {
      'Content-Type': 'text/csv; charset=utf-8',
      'Content-disposition': `attachment; filename=${filename}`
    });
  }

  if(type === "json"){
    res.sendRaw(200, beautifyJSON({ account: historyAccount[0], result: history }, beautify), { 'Content-Type': 'application/json; charset=utf-8' })
  }

  next()
}

// only yhr transactions have a csv output for now
const convert_csv_transactions: CSVConverter = ( history, decimal) =>{
  let convertedArray = []
  for (let i = 0; i < history.length; i++) {
    for (let j = 0; j < history[i].ids.length; j++) {
      let id = history[i].ids[j]
      let tmp = {
        date: history[i].date,
        user: history[i].user,
        id: id,
        quantity: history[i].data[id].quantity,
        totalPrice: history[i].data[id].totalPrice,
        transactions: history[i].data[id].transactions
      }
      let jsonString = JSON.stringify(tmp)
      jsonString.replace(/./g, decimal)
      convertedArray.push(JSON.parse(jsonString))
    }
  }
  return parse(convertedArray, { fields: ["date","user", "id", "quantity", "totalPrice", "transactions" ] })
}