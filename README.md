# DataWars2 Backend

## Info
This is the backend to [DataWars2](https://gitlab.com/Silvers_Gw2/Stats_Frontend) and controls all parts related to accounts

## Running it
Please refer to [Contributing Document](https://gitlab.com/Silvers_Gw2/Stats_Frontend/blob/master/CONTRIBUTING.md)

## Contact
If you have any suggestions just ask and I will try to incorporate them in.

* Silver#5563 on Discord
* Silveress_Golden on reddit  